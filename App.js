import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { AppLoading } from 'expo';
import * as Font from 'expo-font';
import { Asset } from 'expo-asset';
import { store, persistor } from './src/config/store';
import NavMain from './src/routes/menu';

export default class App extends React.Component {
  state = {
    isReady: false,
  }

  async loadAssets() {
    await Promise.all([
      Asset.loadAsync([
        require('./assets/snd/win.mp3'),
        require('./assets/snd/deal.mp3'),
        require('./assets/snd/lose.mp3'),
        require('./assets/snd/button.mp3'),
        require('./assets/snd/discard.mp3'),
        require('./assets/snd/avoid.mp3'),
      ]),
      Font.loadAsync({
        'Work Sans Regular': require('./assets/fonts/WorkSans-Regular.ttf'),
      }),
    ]);
  }

  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this.loadAssets}
          onFinish={() => this.setState({ isReady: true })}
        />
      );
    }

    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <NavMain />
        </PersistGate>
      </Provider>
    );
  }
}
