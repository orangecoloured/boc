export const colors = {
  black: '#171717',
  grey: '#2e2e2e',
  white: '#e8e8e8',
  red: '#c23b22',
};

export const gaps = {
  xsmall: 4,
  small: 8,
  medium: 16,
  big: 32,
};

export const font = {
  small: {
    size: 15,
    height: 21,
  },
  medium: {
    size: 17,
    height: 23,
  },
  big: {
    size: 23,
    height: 29,
  },
};

export const opacities = {
  active: 0.87,
  inactive: 0.54,
  idle: 0.73,
};
