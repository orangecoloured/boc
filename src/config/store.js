import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { persistCombineReducers, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import * as reducers from 'cc.rckt.boc/src/reducers';
import sagaRoot from 'cc.rckt.boc/src/sagas';

const config = {
  key: 'boc',
  blacklist: ['Defaults', 'Sounds'],
  debounce: 500,
  storage,
};

const rootReducer = persistCombineReducers(config, reducers);
const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  rootReducer,
  compose(applyMiddleware(sagaMiddleware))
);

const persistor = persistStore(store, null, () => {
  sagaMiddleware.run(sagaRoot);
});

export { store, persistor };
