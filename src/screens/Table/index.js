import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { BackHandler, View, Animated, Easing } from 'react-native';
import AlertPromise from 'cc.rckt.boc/src/components/AlertPromise';
import { shuffleArray } from 'cc.rckt.boc/src/utils';

import { soundPlay } from 'cc.rckt.boc/src/reducers/sounds/actions';
import { vibrationDo } from 'cc.rckt.boc/src/reducers/vibration/actions';
import { updateBatch, incrementGamesTotal, incrementTurnsAvoided } from 'cc.rckt.boc/src/reducers/stats/actions';

import Cards from './Cards';
import Controls from './Controls';

import style from './style';

class Table extends React.Component {
  state = {};

  componentDidMount() {
    this._isMounted = true;

    this.props.navigation.setParams({
      action: this.gameQuit,
    });

    this._isFocused = this.props.navigation.addListener('didFocus',() => {
      this.props.navigation.setParams({
        title: `Battle  #${this.props.sessionNumber + 1}`,
      });
      BackHandler.addEventListener('hardwareBackPress', this.gameQuit);
      this.gameReset();
    });

    this._isBlurred = this.props.navigation.addListener('willBlur',() => {
      BackHandler.removeEventListener('hardwareBackPress', this.gameQuit);
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
    this._isFocused.remove();
    this._isBlurred.remove();
    BackHandler.removeEventListener('hardwareBackPress', this.gameQuit);
  }

  gameReset = () => {
    this.setState({
      deck: {
        ...this.props.deck,
        active: shuffleArray(this.props.deck.active),
        animate: [0, 1, 2, 3].map(() => new Animated.Value(0)),
      },
      stats: { ...this.props.stats },
      game: { ...this.props.game },
    }, () => {
      this.deckDraw();
    });
  };

  gameQuit = () => {
    if (this.state.deck.discarded.length) {
      AlertPromise(
        {
          title: 'Flee the Battle',
          message: 'Do you want to flee this battle?',
          buttons: [
            {
              label: 'No',
              style: 'cancel',
              state: 'reject',
            },
            {
              label: 'Yes',
              style: 'destructive',
              state: 'resolve',
            },
          ],
        }
      )
      .then(() => {
        this.props.screenProps.rootNavigation.goBack();
      })
      .catch(() => {});
    } else {
      this.props.screenProps.rootNavigation.goBack();
    }

    return true;
  };

  turnAvoid = () => {
    const { game, deck } = this.state;

    this.props.incrementAvoidedNumber();
    this.setState({
      game: {
        ...game,
        turnAvoided: true,
        turnActive: false,
      },
    }, () => {
      this.deckAnimate('avoid', [], () => {
        this.setState({
          deck: {
            ...deck,
            active: shuffleArray([ ...deck.active, ...deck.drawn.filter(item => item) ]),
            drawn: [],
          },
        }, () => {
          this.deckDraw();
        });
      });
    });
  };

  deckAnimate = (action, deckTransitional = [], callback) => {
    const { deck } = this.state;
    const animateArray = [];

    deck.animate.forEach(item => Animated.timing(item).stop());

    this.setState({
      deck: {
        ...deck,
        animate: [0, 1, 2, 3].map(index => {
          if (!deckTransitional.includes(index)) {
            return new Animated.Value(action === 'deal' ? 0 : 1);
          } else {
            return deck.animate[index];
          }
        }),
      },
    }, () => {
      this.state.deck.animate.forEach((item, index) => {
        if (!deckTransitional.includes(index)) {
          animateArray[action === 'deal' ? 'push' : 'unshift'](
            Animated.timing(
              item,
              {
                toValue: action === 'deal' ? 1 : 0,
                duration: 200,
                easing: Easing.out(Easing.quad),
                isInteraction: false,
                useNativeDriver: true,
              }
            )
          );
        }
      });

      Animated.stagger(100, animateArray).start(() => {
        if (this._isMounted && callback) {
          callback();
        }
      });

      switch (action) {
        case 'deal': {
          this.props.doVibration('deal');
          this.props.playSound('deal');
          break;
        }
        case 'avoid': {
          this.props.doVibration('avoid');
          this.props.playSound('avoid');
          break;
        }
        default: {
          return false;
        }
      }
    });
  };

  deckDraw = () => {
    const { deck, game } = this.state;
    const deckTransitional = [];
    const deckActive = [ ...deck.active ];

    const deckDrawn = [0, 1, 2, 3].map(item => {
      if (deck.drawn[item]) {
        deckTransitional.push(item);
        return deck.drawn[item];
      } else {
        return deckActive.splice(0, 1)[0];
      }
    });

    this.setState({
      deck: {
        ...deck,
        active: deckActive,
        drawn: deckDrawn,
      },
      game: {
        ...game,
        cardPicked: false,
      },
    }, () => {
      this.deckAnimate('deal', deckTransitional, () => {
        this.setState({
          game: {
            ...this.state.game,
            turnActive: true,
          },
        });
      });
    });
  };

  deckDiscardAnimate = index => {
    const { deck } = this.state;

    Animated.timing(deck.animate[index]).stop();
    Animated.timing(
      deck.animate[index],
      {
        toValue: 0,
        duration: 200,
        easing: Easing.out(Easing.quad),
        isInteraction: false,
        useNativeDriver: true,
      }
    ).start(() => {
      if (this._isMounted) {
        this.postDiscard(index);
      }
    });
  };

  handleMonster = card => {
    const stats = { ...this.state.stats };

    if (stats.ap) {
      if (stats.al) {
        if (card.value > stats.al) {
          stats.ap = null;
          stats.al = null;
        } else {
          stats.al = card.value - 1;
        }
      } else {
        stats.al = card.value - 1;
      }
    }

    const damage = card.value - (stats.ap || 0);

    if (damage > 0) {
      stats.hp -= damage;
    }

    return {
      game: {
        potionUsed: false,
      },
      stats,
    };
  };

  handlePotion = card => {
    const { stats, game } = this.state;
    const newHp = stats.hp + card.value;

    if (!game.potionUsed) {
      return {
        game: {
          potionUsed: true,
        },
        stats: {
          hp: newHp > 20 ? 20 : newHp,
        },
      };
    } else {
      return null;
    }
  };

  handleWeapon = card => {
    return {
      game: {
        potionUsed: false,
      },
      stats: {
        ap: card.value,
        al: null,
      },
    };
  };

  onDiscard = (card, index) => {
    this.props.doVibration('button');

    const { game, stats, deck } = this.state;

    if (!game.turnActive) {
      return false;
    }

    let newState = null;

    switch (card.suit) {
      case 'h': {
        newState = this.handlePotion(card);
        break;
      }
      case 'd': {
        newState = this.handleWeapon(card);
        break;
      }
      default: {
        newState = this.handleMonster(card);
      }
    }

    this.setState({
      game: {
        ...game,
        ...((newState && newState.game) ? newState.game : {}),
        cardPicked: true,
        turnActive: false,
      },
      stats: {
        ...stats,
        ...((newState && newState.stats) ? newState.stats : {}),
      },
    }, () => {
      this.deckDiscardAnimate(index);
      this.props.playSound('discard');
    });

    if (!deck.discarded.length) {
      this.props.incrementGamesNumber();
    }
  };

  scoreCalculate = won => {
    const { stats, deck } = this.state;
    let score = stats.hp;
    let lastCard = null;

    if (!won) {
      score = deck.drawn.reduce((prev, current) => {
        return prev - (current && (current.suit !== 'h' && current.suit !== 'd') ? current.value : 0);
      }, score);

      score = deck.active.reduce((prev, current) => {
        return prev - (current && (current.suit !== 'h' && current.suit !== 'd') ? current.value : 0);
      }, score);
    } else {
      if (stats.hp >= 20) {
        lastCard = deck.discarded[deck.discarded.length - 1];
        score += (lastCard.suit === 'h' ? lastCard.value : 0);
      }
    }

    return score;
  };

  postDiscard = index => {
    const { stats, game, deck } = this.state;
    const drawn = [ ...deck.drawn ];
    const discarded = [ ...deck.discarded, ...drawn.splice(index, 1, null) ];
    const lost = stats.hp <= 0;
    const won = !lost && (!deck.active.length && drawn.every(item => !item));
    const isTurn = !lost && (drawn.filter(item => item).length === 1 && deck.active.length);

    this.setState({
      deck: {
        ...deck,
        drawn,
        discarded,
      },
      game: {
        ...game,
        ...(isTurn ? { turnAvoided: false } : {}),
        turnActive: !lost && !won && !isTurn,
      },
    }, () => {
      if (isTurn) {
        this.deckDraw();

        return false;
      }

      if (lost) {
        this.deckAnimate('death');
        this.gameOver(false, this.scoreCalculate(false));
      } else if (won) {
        this.gameOver(true, this.scoreCalculate(true));
      }
    });
  };

  gameOver = (won, score) => {
    this.props.doVibration(won ? 'won' : 'lost');
    this.props.playSound(won ? 'win' : 'lose');
    this.props.updateGameStats(won, score);
    this.props.navigation.navigate('Result', {
      won,
      score,
    });
  };

  render() {
    const { stats, deck, game } = this.state;

    if (!deck) {
      return null;
    }

    return(
      <View style={style.container}>
        <Cards
          cards={deck.drawn}
          animate={deck.animate}
          onPress={this.onDiscard}
        />
        <Controls
          deck={deck.active.length}
          onAvoidPress={this.turnAvoid}
          { ...stats }
          { ...game }
        />
      </View>
    );
  }
}

Table.propTypes = {
  navigation: PropTypes.object,
  screenProps: PropTypes.object,
  deck: PropTypes.object,
  stats: PropTypes.object,
  game: PropTypes.object,
  playSound: PropTypes.func,
  doVibration: PropTypes.func,
  updateGameStats: PropTypes.func,
  incrementGamesNumber: PropTypes.func,
  incrementAvoidedNumber: PropTypes.func,
  sessionNumber: PropTypes.number,
};

export default connect(
  state => ({
    deck: state.Defaults.deck,
    stats: state.Defaults.stats,
    game: state.Defaults.game,
    sessionNumber: state.Stats.games.total,
  }),
  dispatch => ({
    playSound: name => dispatch(soundPlay(name)),
    doVibration: patternName => dispatch(vibrationDo(patternName)),
    updateGameStats: (won, score) => dispatch(updateBatch(won, score)),
    incrementGamesNumber: () => dispatch(incrementGamesTotal()),
    incrementAvoidedNumber: () => dispatch(incrementTurnsAvoided()),
  })
)(Table);
