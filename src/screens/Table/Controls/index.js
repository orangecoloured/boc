import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import Gauge from 'cc.rckt.boc/src/components/ui/Gauge';
import ButtonStandard from 'cc.rckt.boc/src/components/ui/buttons/Standard';

import { SvgHearts, SvgDiamonds, SvgCards } from 'cc.rckt.boc/src/components/svg';

import style from './style';

const Controls = ({ deck, onAvoidPress, hp, ap, al, potionUsed, cardPicked, turnAvoided, turnActive }) => (
  <View style={style.wrapper}>
    <Gauge
      styleWrapper={style.gauge}
      value={ap}
      valueAdditional={al}
      icon={height => <SvgDiamonds height={height} />}
    />
    <Gauge
      styleWrapper={style.gauge}
      value={hp}
      icon={height => <SvgHearts height={height} />}
      highlight={potionUsed}
    />
    <Gauge
      styleWrapper={style.gauge}
      value={deck}
      icon={height => <SvgCards height={height} />}
      />
    <ButtonStandard
      styleWrapper={style.buttonAvoidWrapper}
      styleButton={style.buttonAvoid}
      styleText={style.buttonAvoidText}
      label="AVOID"
      onPress={onAvoidPress}
      disabled={cardPicked || turnAvoided || !turnActive}
    />
  </View>
);

Controls.propTypes = {
  deck: PropTypes.number,
  onAvoidPress: PropTypes.func,
  hp: PropTypes.number,
  ap: PropTypes.number,
  al: PropTypes.number,
  potionUsed: PropTypes.bool,
  cardPicked: PropTypes.bool,
  turnAvoided: PropTypes.bool,
  turnActive: PropTypes.bool,
};

export default Controls;
