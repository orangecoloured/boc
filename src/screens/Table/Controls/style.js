import { colors, gaps } from 'cc.rckt.boc/src/config/vars';
import { moderateScale } from 'react-native-size-matters';
import { viewportUnits } from 'cc.rckt.boc/src/utils';

const { vw } = viewportUnits;

export default {
  wrapper: {
    height: 56,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 2 * vw,
    paddingLeft: 2 * vw,
  },
  gauge: {
    marginRight: moderateScale(gaps.medium),
  },
  buttonAvoidWrapper: {
    marginLeft: 'auto',
  },
  buttonAvoid: {
    height: '100%',
    paddingHorizontal: moderateScale(gaps.medium),
    backgroundColor: colors.red,
  },
  buttonAvoidText: {
    color: colors.white,
  },
};
