import { colors } from 'cc.rckt.boc/src/config/vars';

export default {
  container: {
    backgroundColor: colors.black,
    flex: 1,
  },
};
