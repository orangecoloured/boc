import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import CardPlaceholder from './Placeholder';
import Card from './Card';
import { viewportUnits } from 'cc.rckt.boc/src/utils';

import style from './style';

const { vw } = viewportUnits;

const Cards = ({ cards, animate, onPress }) => (
  <View style={style.cardsWrapper}>
    <View style={style.cards}>
      {
        [1, 2, 3, 4].map(item => (
          <View key={`wrapper : placeholder : ${item}`} style={style.cardWrapper}>
            <CardPlaceholder width={47 * vw} />
          </View>
        ))
      }
    </View>
    <View style={style.cards}>
      {
        cards.map((item, index) => {
          return (
            <View key={`wrapper : card : ${index}`} style={style.cardWrapper}>
              {
                item ? (
                  <Card
                    { ...item }
                    width={47 * vw}
                    animate={animate[index]}
                    dir={index % 2 ? 1 : -1}
                    onPress = {() => onPress(item, index)}
                  />
                ) : (
                  null
                )
              }
            </View>
          )
        })
      }
    </View>
  </View>
);

Cards.propTypes = {
  cards: PropTypes.array,
  animate: PropTypes.array,
  onPress: PropTypes.func,
};

export default Cards;
