import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import {
  SvgDiamonds,
  SvgHearts,
  SvgSpades,
  SvgClubs,
  SvgJoker,
  SvgQueen,
  SvgSnake,
  SvgKing,
  SvgJack,
  SvgAce,
} from 'cc.rckt.boc/src/components/svg';
import { colors } from 'cc.rckt.boc/src/config/vars';

import style from './style';

class CardContent extends React.Component {
  dataDisplay = (suit, value) => {
    let dataValue = {};
    let dataSuit = {};

    switch (value) {
      case 11: {
        dataValue =  {
          value: 'J',
          Face: (width, fill) => <SvgJack width={width} fill={fill} />,
        };
        break;
      }
      case 12: {
        dataValue =  {
          value: 'Q',
          Face: (width, fill) => <SvgQueen width={width} />,
        };
        break;
      }
      case 13: {
        dataValue = {
          value: 'K',
          Face: (width, fill) => <SvgKing width={width} />,
        };
        break;
      }
      case 14: {
        dataValue = {
          value: 'A',
          Face: (width, fill) => <SvgAce width={width} fill={fill} />,
        };
        break;
      }
      case 15: {
        dataValue = {
          value: 'J',
          Face: (width, fill, key) => <SvgSnake width={width} key={key} />,
        };
        break;
      }
      default: {
        dataValue = {
          value,
        }
      }
    }

    switch (suit) {
      case 'h':
      case 'd': {
        dataSuit.color = colors.red;
        break;
      }
      default: {
        dataSuit.color = colors.black;
      }
    }

    switch (suit) {
      case 'h': {
        dataSuit.Suit = (width, key) => <SvgHearts width={width} key={key} />;
        break;
      }
      case 'd': {
        dataSuit.Suit = (width, key) => <SvgDiamonds width={width} key={key} />;
        break;
      }
      case 'c': {
        dataSuit.Suit = (width, key) => <SvgClubs width={width} key={key} />;
        break;
      }
      case 's': {
        dataSuit.Suit = (width, key) => <SvgSpades width={width} key={key} />;
        break;
      }
      case 'j': {
        dataSuit.Suit = (width, key) => <SvgJoker width={width} key={key} />;
      }
    }

    return {
      ...dataValue,
      ...dataSuit,
    };
  }

  render() {
    const { suit, value, name, width, blank, styleCustom } = this.props;
    const cardName = `${name} : ${value}`;
    const dataDisplay = this.dataDisplay(suit, value);

    if (blank) {
      return <View style={{ ...style.body, ...styleCustom }} />;
    }

    return (
      <View style={{ ...style.body, ...styleCustom }}>
        <View style={style.innerBody} pointerEvents="none">
          <View style={style.value}>
            <View style={style.suit}>
              {dataDisplay.Suit(width * 0.15, `suit : ${cardName}`)}
            </View>
            <Text style={{ ...style.text, fontSize: width * 0.1, color: dataDisplay.color }}>
              {dataDisplay.value}
            </Text>
          </View>
          {
            dataDisplay.Face ? (
                dataDisplay.Face(width * 0.4, dataDisplay.color)
            ) : (
              <View style={style.pattern}>
                {
                  Array(value).fill().map((item, index) => dataDisplay.Suit(width * 0.175, `patttern : ${index} : ${cardName}`))
                }
              </View>
            )
          }
          <Text style={{ ...style.title, fontSize: width * 0.09, color: dataDisplay.color }} numberOfLines={1}>{cardName}</Text>
        </View>
      </View>
    );
  }
}

CardContent.propTypes = {
  suit: PropTypes.string,
  value: PropTypes.number,
  name: PropTypes.string,
  width: PropTypes.number,
  blank: PropTypes.bool,
  styleCustom: PropTypes.object,
};

export default CardContent;
