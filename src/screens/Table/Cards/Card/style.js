import { colors } from 'cc.rckt.boc/src/config/vars';
import { moderateScale } from 'react-native-size-matters';

export default {
  wrapper: {
    flex: 1,
  },
  bodyWrapper: {
    flex: 1,
  },
  body: {
    borderRadius: moderateScale(4),
    backgroundColor: colors.white,
    flex: 1,
  },
  innerBody: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  value: {
    position: 'absolute',
    top: '3%',
    left: '4%',
    flexDirection: 'column',
    flexWrap: 'nowrap',
    alignItems: 'center',
  },
  suit: {
    marginBottom: '18%',
  },
  text: {
    fontFamily: 'Work Sans Regular',
  },
  title: {
    fontFamily: 'Work Sans Regular',
    position: 'absolute',
    bottom: '5%',
    left: 0,
    width: '100%',
    textAlign: 'center',
  },
  pattern: {
    width: '56%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
};
