import React from 'react';
import PropTypes from 'prop-types';
import { Animated, TouchableNativeFeedback, View } from 'react-native';
import CardContent from './Content';
import { colors } from 'cc.rckt.boc/src/config/vars';

import style from './style';

class Card extends React.Component {
  render() {
    const { animate, dir, onPress, ...cardContent } = this.props;
    const { suit } = this.props;
    const colorRipple = suit === 'h' || suit === 'd' ? colors.red : colors.black;

    return (
      <Animated.View
        style={{
          ...style.wrapper,
          opacity: animate,
          transform: [{
            translateX: animate.interpolate({
              inputRange: [0, 1],
              outputRange: [dir * 20, 0]
            }),
          }],
        }}
      >
        <TouchableNativeFeedback
          background={TouchableNativeFeedback.Ripple(colorRipple, true)}
          useForeground={true}
          onPress={onPress}
        >
          <View style={style.bodyWrapper}>
            <CardContent {...cardContent} />
          </View>
        </TouchableNativeFeedback>
      </Animated.View>
    );
  }
}

Card.propTypes = {
  animate: PropTypes.object,
  dir: PropTypes.number,
  onPress: PropTypes.func,
  suit: PropTypes.string,
};

export default Card;
