import { viewportUnits } from 'cc.rckt.boc/src/utils';

const { vw } = viewportUnits;

export default {
  cardsWrapper: {
    flex: 1,
    marginVertical: -vw,
  },
  cards: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: vw,
  },
  cardWrapper: {
    width: '50%',
    height: '50%',
    padding: vw,
  },
};
