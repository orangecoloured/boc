import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { SvgLogo } from 'cc.rckt.boc/src/components/svg';

import { colors } from 'cc.rckt.boc/src/config/vars';

import style from './style';

const Placeholder = props => (
  <View style={style.placeholder}>
    <SvgLogo
      width={props.width * 0.2}
      fill={colors.black}
    />
  </View>
);

Placeholder.propTypes = {
    width: PropTypes.number,
};

export default Placeholder;
