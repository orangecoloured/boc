import { colors } from 'cc.rckt.boc/src/config/vars';
import { moderateScale } from 'react-native-size-matters';

export default {
  placeholder: {
    backgroundColor: colors.grey,
    borderRadius: moderateScale(4),
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
};
