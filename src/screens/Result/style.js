import { colors, gaps, font } from 'cc.rckt.boc/src/config/vars';
import { moderateScale } from 'react-native-size-matters';
import { viewportUnits } from 'cc.rckt.boc/src/utils';

const { vw } = viewportUnits;

export default {
  container: {
    backgroundColor: colors.red,
    flex: 1,
  },
  text: {
    fontFamily: 'Work Sans Regular',
    color: colors.white,
    fontSize: moderateScale(font.big.size),
    lineHeight: moderateScale(font.big.big),
    textAlign: 'center',
  },
  image: {
    marginVertical: gaps.big,
    alignItems: 'center',
  },
  top: {
    flex: 1,
    justifyContent: 'center',
  },
  bottom: {
    padding: vw,
    flexDirection: 'row',
  },
  buttonWrapper: {
    padding: vw,
    paddingTop: 0,
    flex: 1,
  },
  button: {
    paddingVertical: moderateScale(gaps.medium),
  },
  buttonText: {
    textAlign: 'center',
    fontSize: moderateScale(font.medium.size),
    lineHeight: moderateScale(font.medium.height),
  },
};
