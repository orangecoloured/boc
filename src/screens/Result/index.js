import React from 'react';
import PropTypes from 'prop-types';
import { BackHandler, Dimensions, View, Text } from 'react-native';
import { SvgWin, SvgLose } from 'cc.rckt.boc/src/components/svg';
import ButtonStandard from 'cc.rckt.boc/src/components/ui/buttons/Standard';



import style from './style';

const { width } = Dimensions.get('window');

class Result extends React.Component {
  componentDidMount () {
    this._isFocused = this.props.navigation.addListener('didFocus',() => {
      BackHandler.addEventListener('hardwareBackPress', this.goToMenu);
    });

    this._isBlurred = this.props.navigation.addListener('willBlur',() => {
      BackHandler.removeEventListener('hardwareBackPress', this.goToMenu);
    });
  }

  componentWillUnmount() {
    this._isFocused.remove();
    this._isBlurred.remove();
    BackHandler.removeEventListener('hardwareBackPress', this.goToMenu);
  }

  goToMenu = () => {
    this.props.screenProps.rootNavigation.goBack();

    return true;
  }

  gameRestart = () => {
    this.props.navigation.goBack();
  }

  render() {
    const { score, won } = this.props.navigation.state.params;

    return(
      <View style={style.container}>
        <View style={style.top}>
          <Text style={style.text} numberOfLines={1}>
            You {!won ? 'died in' : 'survived'} this battle{!won ? '.' : '!'}
          </Text>
          <View style={style.image}>
            {
              !won ? (
                <SvgLose width={width * 0.38} />
              ) : (
                <SvgWin width={width * 0.38} />
              )
            }
          </View>
          <Text style={style.text} numberOfLines={1}>
            Score : {score}
          </Text>
        </View>
        <View style={style.bottom}>
          <ButtonStandard
            styleWrapper={style.buttonWrapper}
            styleButton={style.button}
            styleText={style.buttonText}
            label="Enough"
            onPress={this.goToMenu}
          />
          <ButtonStandard
            styleWrapper={style.buttonWrapper}
            styleButton={style.button}
            styleText={style.buttonText}
            label="Again!"
            onPress={this.gameRestart}
          />
        </View>
      </View>
    );
  }
}

Result.propTypes = {
  navigation: PropTypes.object,
  screenProps: PropTypes.object,
};

export default Result;
