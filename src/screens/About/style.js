import { colors, gaps } from 'cc.rckt.boc/src/config/vars';
import { moderateScale } from 'react-native-size-matters';

export default {
  container: {
    backgroundColor: colors.black,
    flex: 1,
  },
  textWrapper: {
    paddingHorizontal: moderateScale(gaps.medium),
    paddingTop: moderateScale(gaps.medium),
  },
  link: {
    textDecorationLine: 'underline',
  },
};
