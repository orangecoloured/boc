import React from 'react';
import Constants from 'expo-constants';
import { View, ScrollView, Text, Linking } from 'react-native';

import { TextParagraph, TextHeader } from 'cc.rckt.boc/src/components/typography';

import style from './style';

const About = () => (
  <ScrollView style={style.container}>
    <View style={style.textWrapper}>
      <TextHeader>
        Application
      </TextHeader>
      <TextParagraph>
        Version {Constants.manifest.version}.
      </TextParagraph>
      <TextParagraph>
        Created by Alexander Petcoglo, <Text style={style.link} onPress={() => Linking.openURL('https://rckt.cc')}>https://rckt.cc</Text>.
      </TextParagraph>
      <TextParagraph>
        Any questions or feedback you might have you can send to <Text style={style.link} onPress={() => Linking.openURL('mailto:mail@rckt.cc')}>mail@rckt.cc</Text>.
      </TextParagraph>
      <TextHeader>
        References
      </TextHeader>
      <TextParagraph>
        The rules of this game are a bit altered version of &laquo;Scoundrel&raquo;, a game designed by Zach Gage and Kurt Bieg.
      </TextParagraph>
      <TextParagraph>
        The visual style is inspired by &laquo;Donsol&raquo;, a game designed by John Eternal and developed by Hundred Rabbits.
      </TextParagraph>
      <TextParagraph>
        Sounds are taken from &laquo;Alien: Isolation&raquo;, a game developed by Creative Assembly.
      </TextParagraph>
      <TextParagraph>
        There is an adaptation called &laquo;Diamonds & Dragons&raquo; that goes well with a physical deck of cards, created by Rory Philstrom.
      </TextParagraph>
    </View>
  </ScrollView>
);

export default About;
