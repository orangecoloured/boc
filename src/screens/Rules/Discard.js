import React from 'react';
import PropTypes from 'prop-types';
import { View, Animated, Easing } from 'react-native';
import Card from 'cc.rckt.boc/src/screens/Table/Cards/Card/Content';
import { TextParagraph } from 'cc.rckt.boc/src/components/typography';
import { shuffleArray } from 'cc.rckt.boc/src/utils';

import style from './style';

class DiscardExample extends React.Component {
  state = {
    active: true,
    cards: [0, 1, 2, 3].map(() => new Animated.Value(1)),
    paragraph: new Animated.Value(1),
    queue: 0,
    sequences: [
      ['discard', 3, 500],
      ['deal', 4, 100],
      ['flush', 4, 100],
      ['deal', 4, 100],
    ],
    sequenceInterval: 2000,
  };

  componentDidMount() {
    this._isMounted = true;

    this._isFocused = this.props.navigation.addListener('didFocus',() => {
      if (this._isMounted) {
        this.setState({
          active: true,
        }, () => this.doSequence());
      }
    });

    this._isBlurred = this.props.navigation.addListener('willBlur',() => {
      if (this._isMounted) {
        this.setState({
          active: false,
        });
      }
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  animateParagraph = () => {
    const { queue, paragraph } = this.state;

    Animated.timing(paragraph).stop();
    Animated.timing(
      paragraph,
      {
        toValue: queue === 0 || queue === 3 ? 1 : 0,
        duration: 200,
        easing: Easing.out(Easing.quad),
        isInteraction: false,
        useNativeDriver: true,
      }
    ).start();
  }

  doSequence = () => {
    const { queue, sequences, sequenceInterval } = this.state;

    setTimeout(() => {
      if (this._isMounted && this.state.active) {
        this.animateCards( ...sequences[queue] );
        this.animateParagraph();

        this.setState({
          queue: queue < 3 ? queue + 1 : 0,
        });
      }
    }, sequenceInterval);
  }

  animateCards = (action, number, interval, callback = this.doSequence) => {
    const animateArray = [];
    let cards = [ ...this.state.cards ];

    cards.forEach(item => Animated.timing(item).stop());
    if (action === 'discard') {
      cards = shuffleArray(cards);
    }
    cards.length = number;
    cards.forEach(item => {
      animateArray[action === 'deal' ? 'push' : 'unshift'](
        Animated.timing(
          item,
          {
            toValue: action === 'deal' ? 1 : 0,
            duration: 200,
            easing: Easing.out(Easing.quad),
            isInteraction: false,
            useNativeDriver: true,
          }
        )
      );
    });

    Animated.stagger(interval, animateArray).start(() => {
      if (this._isMounted && callback) {
        callback();
      }
    });
  }

  createCard = (value, index) => (
    <View key={`discard : card : ${index}`} style={style.animatedCardWrapper} aspectRatio={0.77}>
      <Animated.View
        style={{
          ...style.animatedCard,
          opacity: value,
          transform: [{
            translateX: value.interpolate({
              inputRange: [0, 1],
              outputRange: [20, 0],
            }),
          }],
        }}
      >
        <Card blank />
      </Animated.View>
    </View>
  );

  createParagraph = (children, index) => {
    const odd = index % 2;
    const { paragraph } = this.state;

    return (
      <Animated.View
        key={`discard : paragraph : ${index}`}
        style={{
          ...style.animatedCardsParagraphWrapper,
          opacity: paragraph.interpolate({
            inputRange: [0, 1],
            outputRange: !odd ? [0, 1] : [1, 0],
          }),
          transform: [{
            translateY: paragraph.interpolate({
              inputRange: [0, 1],
              outputRange: !odd ? [-40, 0] : [0, 40],
            }),
          }],
        }}
      >
        <TextParagraph styleCustom={style.animatedCardsParagraph}>
          {children}
        </TextParagraph>
      </Animated.View>
    );
  }

  render() {
    const { cards } = this.state;

    return(
      <View style={{ ...style.paragraphView, ...style.row }}>
        <View>
          <View>
            <View style={style.animatedCardsRow}>
              <View style={style.animatedCardWrapper} aspectRatio={0.77}>
                <Card blank styleCustom={style.placeholder} />
              </View>
              <View style={style.animatedCardWrapper} aspectRatio={0.77}>
                <Card blank styleCustom={style.placeholder} />
              </View>
            </View>
            <View style={{ ...style.animatedCardsRow, ...style.noMarginBottom }}>
              <View style={style.animatedCardWrapper} aspectRatio={0.77}>
                <Card blank styleCustom={style.placeholder} />
              </View>
              <View style={style.animatedCardWrapper} aspectRatio={0.77}>
                <Card blank styleCustom={style.placeholder} />
              </View>
            </View>
          </View>
          <View style={style.animatedCardsOverlay}>
            <View style={style.animatedCardsRow}>
              {[
                this.createCard(cards[0], 0),
                this.createCard(cards[1], 1)
              ]}
            </View>
            <View style={{ ...style.animatedCardsRow, ...style.noMarginBottom }}>
              {[
                this.createCard(cards[2], 2),
                this.createCard(cards[3], 3)
              ]}
            </View>
          </View>
        </View>
        <View style={style.animatedCardsDescription}>
          {[
            this.createParagraph('Pick three cards and complete the turn.', 0),
            this.createParagraph('Avoid the cards and return them to the deck.', 1),
          ]}
        </View>
      </View>
    );
  }
}

DiscardExample.propTypes = {
  navigation: PropTypes.object,
};

export default DiscardExample;
