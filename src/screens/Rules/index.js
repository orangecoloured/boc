import React from 'react';
import PropTypes from 'prop-types';
import { View, ScrollView } from 'react-native';
import { viewportUnits } from 'cc.rckt.boc/src/utils';
import Card from 'cc.rckt.boc/src/screens/Table/Cards/Card/Content';
import { TextParagraph, TextHeader } from 'cc.rckt.boc/src/components/typography';
import DiscardExample from './Discard';
import {
  SvgDiamonds,
  SvgHearts,
  SvgSpades,
  SvgClubs,
} from 'cc.rckt.boc/src/components/svg';
import { colors } from 'cc.rckt.boc/src/config/vars';

import style from './style';

const { vw } = viewportUnits;

const cardSmallWidth = 20 * vw;
const cardMediumWidth = 28 * vw;

const suitSize = 8 * vw;

const Rules = props => (
  <ScrollView style={style.container}>
    <View style={style.textWrapper}>
      <TextHeader>
        Setup
      </TextHeader>
      <TextParagraph>
        Battle of Cards is a rogue-like card game. It is played with a standard deck of cards, Jokers included.
      </TextParagraph>
      <TextParagraph>
        The Queen, the King, the Ace of Hearts accompanied by the Jack, the Queen and the King of Diamonds make your party. The rest of the deck is what your party will have to face during the battle.
      </TextParagraph>
      <View style={{ ...style.paragraphView, ...style.row, ...style.alignCenter }}>
        <View style={style.row}>
          <View style={style.cardSmall} aspectRatio={0.7}>
            <Card
              suit="s"
              value={2}
              name="Fallen"
              width={cardSmallWidth}
            />
          </View>
          <View style={{ ...style.cardSmall, ...style.cardSmallOverlay }} aspectRatio={0.7}>
            <Card
              suit="c"
              value={2}
              name="Scavenger"
              width={cardSmallWidth}
            />
          </View>
        </View>
        <View style={style.rowSpacer}><TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>...</TextParagraph></View>
        <View style={style.row}>
          <View style={style.cardSmall} aspectRatio={0.7}>
            <Card
              suit="s"
              value={14}
              name="Butcher"
              width={cardSmallWidth}
            />
          </View>
          <View style={{ ...style.cardSmall, ...style.cardSmallOverlay }} aspectRatio={0.7}>
            <Card
              suit="c"
              value={14}
              name="Sir Gorash"
              width={cardSmallWidth}
            />
          </View>
        </View>
        <View style={style.rowSpacer}><TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>+</TextParagraph></View>
        <View style={style.row}>
          <View style={style.cardSmall} aspectRatio={0.7}>
            <Card
              suit="j"
              value={15}
              name="Twin Serpent"
              width={cardSmallWidth}
            />
          </View>
          <View style={{ ...style.cardSmall, ...style.cardSmallOverlay }} aspectRatio={0.7}>
            <Card
              suit="j"
              value={15}
              name="Twin Serpent"
              width={cardSmallWidth}
            />
          </View>
        </View>
      </View>
      <View style={{ ...style.paragraphView, ...style.row, ...style.alignCenter }}>
        <View style={style.row}>
          <View style={style.cardSmall} aspectRatio={0.7}>
            <Card
              suit="h"
              value={2}
              name="Small Potion"
              width={cardSmallWidth}
            />
          </View>
          <View style={{ ...style.cardSmall, ...style.cardSmallOverlay }} aspectRatio={0.7}>
            <Card
              suit="d"
              value={2}
              name="Dagger"
              width={cardSmallWidth}
            />
          </View>
        </View>
        <View style={style.rowSpacer}><TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>...</TextParagraph></View>
        <View style={style.row}>
          <View style={style.cardSmall} aspectRatio={0.7}>
            <Card
              suit="h"
              value={10}
              name="Big Potion"
              width={cardSmallWidth}
            />
          </View>
          <View style={{ ...style.cardSmall, ...style.cardSmallOverlay }} aspectRatio={0.7}>
            <Card
              suit="d"
              value={10}
              name="Great Sword"
              width={cardSmallWidth}
            />
          </View>
        </View>
        <View style={style.rowSpacer}><TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>+</TextParagraph></View>
        <View style={style.row}>
          <View style={style.cardSmall} aspectRatio={0.7}>
            <Card
              suit="h"
              value={11}
              name="Priest"
              width={cardSmallWidth}
            />
          </View>
          <View style={{ ...style.cardSmall, ...style.cardSmallOverlay }} aspectRatio={0.7}>
            <Card
              suit="d"
              value={14}
              name="Palladin"
              width={cardSmallWidth}
            />
          </View>
        </View>
      </View>
      <TextHeader>
        Cards
      </TextHeader>
      <TextParagraph>
        All the Black Cards are monsters. Their damage is equal to their ordered value: 2 is 2, ..., 10 is 10, Jack is 11, Queen is 12, King is 13, Ace is 14 and Joker is 15.
      </TextParagraph>
      <View style={{ ...style.paragraphView, ...style.row, ...style.alignCenter }}>
        <View style={style.cardMedium} aspectRatio={0.7}>
          <Card
            suit="c"
            value={6}
            name="Demon"
            width={cardMediumWidth}
          />
        </View>
        <View style={style.rowSpacer}>
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftMedium }}>
            This monster is called Demon and the maximum damage it can make equals 6 points.
          </TextParagraph>
        </View>
      </View>
      <TextParagraph>
        The Diamonds are weapons. Each weapon does as much damage as its value. The Ace of Diamonds is a Paladin and makes up to 14 points of damage.
      </TextParagraph>
      <View style={{ ...style.paragraphView, ...style.row, ...style.alignCenter }}>
        <View style={style.cardMedium} aspectRatio={0.7}>
          <Card
            suit="d"
            value={9}
            name="Maul"
            width={cardMediumWidth}
          />
        </View>
        <View style={style.rowSpacer}>
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftMedium }}>
            This weapon is called Maul and it can make up to 9 points of damage.
          </TextParagraph>
        </View>
      </View>
      <TextParagraph>
        The Hearts are health potions. Their effect is equal to their value.
      </TextParagraph>
      <View style={{ ...style.paragraphView, ...style.row, ...style.alignCenter }}>
        <View style={style.cardMedium} aspectRatio={0.7}>
          <Card
            suit="h"
            value={11}
            name="Priest"
            width={cardMediumWidth}
          />
        </View>
        <View style={style.rowSpacer}>
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftMedium }}>
            This is a Priest and it can heal the player by 11 points.
          </TextParagraph>
        </View>
      </View>
      <TextHeader>
        Turns
      </TextHeader>
      <TextParagraph>
        You start with 20 health points.
      </TextParagraph>
      <View style={{ ...style.paragraphView, ...style.row, ...style.alignCenter }}>
        <SvgHearts width={suitSize} />
        <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
          20
        </TextParagraph>
      </View>
      <TextParagraph>
        On every turn you have 4 cards in front of you. You may avoid them if you wish. In this case all four cards go back to the deck and the deck is shuffled. While you may avoid cards as many times as you want, you may not do this two times in a row. If you choose not to avoid the cards, one by one, you must face 3 of them.
      </TextParagraph>
      <DiscardExample navigation={props.navigation} />
      <TextParagraph>
        If you pick a weapon, you replace the current one you have. Also by doing so you reset your attack limit.
      </TextParagraph>
      <View style={{ ...style.paragraphView, ...style.row, ...style.flexStart }}>
        <View>
          <View style={{ ...style.row, ...style.alignCenter }}>
            <SvgDiamonds width={suitSize} />
            <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
              7 / 5
            </TextParagraph>
          </View>
        </View>
        <View style={{ ...style.marginHorizontalMedium, ...style.rowHeightSuit, ...style.justifyCenter }}>
          <TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>+</TextParagraph>
        </View>
        <View style={{ ...style.row, ...style.alignCenter }}>
          <SvgDiamonds width={suitSize} />
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
            4
          </TextParagraph>
        </View>
        <View style={{ ...style.marginHorizontalMedium, ...style.rowHeightSuit, ...style.justifyCenter }}>
          <TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>=</TextParagraph>
        </View>
        <View>
          <View style={{ ...style.row, ...style.alignCenter }}>
            <SvgDiamonds width={suitSize} />
            <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
              4 / -
            </TextParagraph>
          </View>
        </View>
      </View>
      <TextParagraph>
        If you pick a health potion, its number is added to your health. You may not use multiple potions in a row. Only the first one is gained in health points, the rest of the potions you pull are simply discarded. You must pick a non-potion card before another potion can take effect. Your health points may not go above 20.
      </TextParagraph>
      <View style={{ ...style.paragraphView, ...style.row }}>
        <View style={style.alignCenter}>
          <SvgHearts width={suitSize} />
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginTopSmall }}>
            9
          </TextParagraph>
        </View>
        <View style={{ ...style.marginHorizontalMedium, ...style.rowHeightSuit, ...style.justifyCenter }}>
          <TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>+</TextParagraph>
        </View>
        <View style={style.alignCenter}>
          <SvgHearts width={suitSize} />
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginTopSmall }}>
            5
          </TextParagraph>
        </View>
        <View style={{ ...style.marginHorizontalMedium, ...style.rowHeightSuit, ...style.justifyCenter }}>
          <TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>+</TextParagraph>
        </View>
        <View style={style.alignCenter}>
          <SvgHearts width={suitSize} />
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginTopSmall }}>
            7
          </TextParagraph>
        </View>
        <View style={{ ...style.marginHorizontalMedium, ...style.rowHeightSuit, ...style.justifyCenter }}>
          <TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>=</TextParagraph>
        </View>
        <View style={style.alignCenter}>
          <SvgHearts width={suitSize} />
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginTopSmall }}>
            14
          </TextParagraph>
        </View>
      </View>
      <View style={style.columnSpacerMedium} />
      <View style={{ ...style.paragraphView, ...style.row }}>
        <View style={style.alignCenter}>
          <SvgHearts width={suitSize} />
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginTopSmall }}>
            9
          </TextParagraph>
        </View>
        <View style={{ ...style.marginHorizontalMedium, ...style.rowHeightSuit, ...style.justifyCenter }}>
          <TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>+</TextParagraph>
        </View>
        <View style={style.alignCenter}>
          <SvgHearts width={suitSize} />
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginTopSmall }}>
            5
          </TextParagraph>
        </View>
        <View style={{ ...style.marginHorizontalMedium, ...style.rowHeightSuit, ...style.justifyCenter }}>
          <TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>+</TextParagraph>
        </View>
        <View style={style.alignCenter}>
          <SvgDiamonds width={suitSize} fill={colors.grey} />
          <View style={style.columnSpacerSmall} />
          <SvgClubs width={suitSize} fill={colors.grey} />
          <View style={style.columnSpacerSmall} />
          <SvgSpades width={suitSize} fill={colors.grey} />
        </View>
        <View style={{ ...style.marginHorizontalMedium, ...style.rowHeightSuit, ...style.justifyCenter }}>
          <TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>+</TextParagraph>
        </View>
        <View style={style.alignCenter}>
          <SvgHearts width={suitSize} />
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginTopSmall }}>
            7
          </TextParagraph>
        </View>
        <View style={{ ...style.marginHorizontalMedium, ...style.rowHeightSuit, ...style.justifyCenter }}>
          <TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>=</TextParagraph>
        </View>
        <View style={style.alignCenter}>
          <SvgHearts width={suitSize} />
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginTopSmall }}>
            20
          </TextParagraph>
        </View>
      </View>
      <TextParagraph>
        If you pick a monster, you either fight it barehanded or with a weapon, if you have one.
      </TextParagraph>
      <TextParagraph>
        If you choose to fight the monster barehanded, its full value is subtracted from your health. If you have a weapon, its value is subtracted from the monster&#39;s value and any remaining value is subtracted from your health. The monster&#39;s value minus one point becomes your new attack limit.
      </TextParagraph>
      <View style={{ ...style.paragraphView, ...style.row, ...style.alignCenter }}>
        <View style={{ ...style.row, ...style.alignCenter }}>
          <SvgHearts width={suitSize} />
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
            17
          </TextParagraph>
        </View>
        <View style={style.marginHorizontalMedium}>
          <TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>+</TextParagraph>
        </View>
        <View style={{ ...style.row, ...style.alignCenter }}>
          <SvgSpades width={suitSize} fill={colors.white} />
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
            6
          </TextParagraph>
        </View>
        <View style={style.marginHorizontalMedium}>
          <TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>=</TextParagraph>
        </View>
        <View style={{ ...style.row, ...style.alignCenter }}>
          <SvgHearts width={suitSize} />
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
            11
          </TextParagraph>
        </View>
      </View>
      <View style={style.columnSpacerMedium} />
      <View style={{ ...style.paragraphView, ...style.row, ...style.flexStart }}>
        <View>
          <View style={{ ...style.row, ...style.alignCenter }}>
            <SvgHearts width={suitSize} />
            <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
              17
            </TextParagraph>
          </View>
          <View style={style.columnSpacerMedium} />
          <View style={{ ...style.row, ...style.alignCenter }}>
            <SvgDiamonds width={suitSize} />
            <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall, ...style.marginRightNegative }}>
              3 / 9
            </TextParagraph>
          </View>
        </View>
        <View style={{ ...style.marginHorizontalMedium, ...style.rowHeightSuit, ...style.justifyCenter }}>
          <TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>+</TextParagraph>
        </View>
        <View style={{ ...style.row, ...style.alignCenter }}>
          <SvgSpades width={suitSize} fill={colors.white} />
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
            6
          </TextParagraph>
        </View>
        <View style={{ ...style.marginHorizontalMedium, ...style.rowHeightSuit, ...style.justifyCenter }}>
          <TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>=</TextParagraph>
        </View>
        <View>
          <View style={{ ...style.row, ...style.alignCenter }}>
            <SvgHearts width={suitSize} />
            <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
              14
            </TextParagraph>
          </View>
          <View style={style.columnSpacerMedium} />
          <View style={{ ...style.row, ...style.alignCenter }}>
            <SvgDiamonds width={suitSize} />
            <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
              3 / 5
            </TextParagraph>
          </View>
        </View>
      </View>
      <TextParagraph>
        You retain your weapons until they are replaced or shattered. Once a weapon is used on a monster, it can only be used to slay monsters of a lower value than the previous monster it had slain. If your attack limit is lower than the monster&#39;s value, then you lose your weapon and have to face the monster barehanded.
      </TextParagraph>
      <View style={{ ...style.paragraphView, ...style.row, ...style.flexStart }}>
        <View>
          <View style={{ ...style.row, ...style.alignCenter }}>
            <SvgHearts width={suitSize} />
            <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
              17
            </TextParagraph>
          </View>
          <View style={style.columnSpacerMedium} />
          <View style={{ ...style.row, ...style.alignCenter }}>
            <SvgDiamonds width={suitSize} />
            <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall, ...style.marginRightNegative }}>
              3 / 2
            </TextParagraph>
          </View>
        </View>
        <View style={{ ...style.marginHorizontalMedium, ...style.rowHeightSuit, ...style.justifyCenter }}>
          <TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>+</TextParagraph>
        </View>
        <View style={{ ...style.row, ...style.alignCenter }}>
          <SvgSpades width={suitSize} fill={colors.white} />
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
            6
          </TextParagraph>
        </View>
        <View style={{ ...style.marginHorizontalMedium, ...style.rowHeightSuit, ...style.justifyCenter }}>
          <TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>=</TextParagraph>
        </View>
        <View>
          <View style={{ ...style.row, ...style.alignCenter }}>
            <SvgHearts width={suitSize} />
            <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
              11
            </TextParagraph>
          </View>
          <View style={style.columnSpacerMedium} />
          <View style={{ ...style.row, ...style.alignCenter }}>
            <SvgDiamonds width={suitSize} />
            <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
              - / -
            </TextParagraph>
          </View>
        </View>
      </View>
      <TextParagraph>
        Once you&#39;ve picked 3 cards, your turn is complete. You&#39;ll face the fourth card in the next turn.
      </TextParagraph>
      <TextParagraph>
        The session ends when either your life reaches zero or below or you make your way through the deck.
      </TextParagraph>
      <TextHeader>
        Scoring
      </TextHeader>
      <TextParagraph>
        If your life has reached zero or below, all the values of the remaining monsters in the deck are subtracted from your health points. The result is your score.
      </TextParagraph>
      <View style={{ ...style.paragraphView, ...style.row, ...style.flexStart }}>
        <View style={{ ...style.row, ...style.alignCenter }}>
          <SvgHearts width={suitSize} />
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
            -2
          </TextParagraph>
        </View>
        <View style={{ ...style.marginHorizontalMedium, ...style.rowHeightSuit, ...style.justifyCenter }}>
          <TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>+</TextParagraph>
        </View>
        <View>
          <View style={{ ...style.row, ...style.alignCenter }}>
            <SvgClubs width={suitSize} fill={colors.white} />
            <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
              7
            </TextParagraph>
          </View>
          <View style={style.columnSpacerMedium} />
          <View style={{ ...style.row, ...style.alignCenter }}>
            <SvgSpades width={suitSize} fill={colors.white} />
            <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
              4
            </TextParagraph>
          </View>
        </View>
        <View style={{ ...style.marginHorizontalMedium, ...style.rowHeightSuit, ...style.justifyCenter }}>
          <TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>=</TextParagraph>
        </View>
        <View style={{ ...style.rowHeightSuit, ...style.justifyCenter }}>
          <TextParagraph styleCustom={style.rowParagraph}>-13</TextParagraph>
        </View>
      </View>
      <TextParagraph>
        If you have made your way through the entire deck, your health points are your score. Or if your health is 20, and your last card was a potion, your score is the sum of your health points and the value of that health potion.
      </TextParagraph>
      <View style={{ ...style.paragraphView, ...style.row, ...style.flexStart }}>
        <View style={{ ...style.row, ...style.alignCenter }}>
          <SvgHearts width={suitSize} />
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
            20
          </TextParagraph>
        </View>
        <View style={{ ...style.marginHorizontalMedium, ...style.rowHeightSuit, ...style.justifyCenter }}>
          <TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>+</TextParagraph>
        </View>
        <View style={{ ...style.row, ...style.alignCenter }}>
          <SvgHearts width={suitSize} />
          <TextParagraph styleCustom={{ ...style.rowParagraphInfo, ...style.marginLeftSmall }}>
            4
          </TextParagraph>
        </View>
        <View style={{ ...style.marginHorizontalMedium, ...style.rowHeightSuit, ...style.justifyCenter }}>
          <TextParagraph styleCustom={{ ...style.rowParagraph, ...style.paragraphBig }}>=</TextParagraph>
        </View>
        <View style={{ ...style.rowHeightSuit, ...style.justifyCenter }}>
          <TextParagraph styleCustom={style.rowParagraph}>24</TextParagraph>
        </View>
      </View>
    </View>
  </ScrollView>
);

Rules.propTypes = {
  navigation: PropTypes.object,
};

export default Rules;
