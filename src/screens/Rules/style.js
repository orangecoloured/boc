import { colors, gaps, font } from 'cc.rckt.boc/src/config/vars';
import { moderateScale } from 'react-native-size-matters';
import { viewportUnits } from 'cc.rckt.boc/src/utils';

const { vw } = viewportUnits;

const cardSmallWidth = 20;
const cardMediumWidth = 28;

const suitSize = 8 * vw;

export default {
  container: {
    backgroundColor: colors.black,
    flex: 1,
  },
  textWrapper: {
    paddingHorizontal: moderateScale(gaps.medium),
    paddingTop: moderateScale(gaps.small),
  },

  row: {
    flexDirection: 'row',
  },
  rowSpacer: {
    flex: 1,
  },
  flexStart: {
    alignItems: 'flex-start',
  },
  rowParagraph: {
    marginBottom: 0,
    textAlign: 'center',
  },
  rowParagraphInfo: {
    marginBottom: 0,
  },
  paragraphView: {
    marginBottom: moderateScale(gaps.medium),
  },
  alignCenter: {
    alignItems: 'center',
  },
  justifyCenter: {
    justifyContent: 'center',
  },

  cardSmall: {
    width: cardSmallWidth * vw,
  },
  cardMedium: {
    width: cardMediumWidth * vw,
  },
  cardSmallOverlay: {
    width: (cardSmallWidth + (cardSmallWidth * 0.0375)) * vw,
    padding: 0.5 * vw,
    borderRadius: moderateScale(5.5),
    backgroundColor: colors.black,
    marginLeft: (-cardSmallWidth * 0.775) * vw,
    marginTop: -0.5 * vw,
  },
  discardExample: {
    flexDirection: 'row',
  },
  animatedCardsRow: {
    flexDirection: 'row',
    marginBottom: 2 * vw,
  },
  noMarginBottom: {
    marginBottom: 0,
  },
  animatedCardWrapper: {
    paddingRight: 2 * vw,
    width: cardSmallWidth * vw,
  },
  animatedCard: {
    flex: 1,
  },
  animatedCardsDescription: {
    flex: 1,
    height: '100%',
  },
  animatedCardsParagraphWrapper: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
  },
  animatedCardsParagraph: {
    marginBottom: 0,
    marginLeft: moderateScale(gaps.medium) - 2 * vw,
  },
  placeholder: {
    backgroundColor: colors.grey,
  },
  animatedCardsOverlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  },

  paragraphBig: {
    fontSize: font.big.size,
    lineHeight: font.big.height,
  },

  marginTopSmall: {
    marginTop: moderateScale(gaps.small),
  },
  marginLeftSmall: {
    marginLeft: moderateScale(gaps.small),
  },
  marginLeftMedium: {
    marginLeft: moderateScale(gaps.medium),
  },
  marginRightNegative: {
    marginRight: -gaps.big,
  },
  marginHorizontalMedium: {
    marginHorizontal: moderateScale(gaps.medium),
  },

  columnSpacerSmall: {
    height: moderateScale(gaps.small),
  },
  columnSpacerMedium: {
    height: moderateScale(gaps.medium),
  },

  rowHeightSuit: {
    height: suitSize,
  },
};
