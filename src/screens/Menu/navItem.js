import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { Dimensions, Text, TouchableOpacity } from 'react-native';
import { moderateScale } from 'react-native-size-matters';

import { soundPlay } from 'cc.rckt.boc/src/reducers/sounds/actions';
import { vibrationDo } from 'cc.rckt.boc/src/reducers/vibration/actions';

import style from './style';

const { width } = Dimensions.get('window');

const NavItem = props => (
  <TouchableOpacity
    style={style.control}
    onPress={() => {
      props.doVibration('button');
      props.playSound('button');
      props.onPress();
    }}
  >
    {
      React.cloneElement(props.children, { width: moderateScale(width * 0.1) })
    }
    <Text
      style={style.controlText}
      numberOfLines={1}
      ellipsizeMode="clip"
    >{props.title}</Text>
  </TouchableOpacity>
);

NavItem.propTypes = {
  children: PropTypes.object,
  onPress: PropTypes.func,
  playSound: PropTypes.func,
  doVibration: PropTypes.func,
  title: PropTypes.string,
};

export default connect(
  null,
  dispatch => ({
    playSound: name => dispatch(soundPlay(name)),
    doVibration: patternName => dispatch(vibrationDo(patternName)),
  })
)(NavItem);
