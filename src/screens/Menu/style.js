import { Dimensions } from 'react-native';
import { colors, gaps, font } from 'cc.rckt.boc/src/config/vars';
import { moderateScale } from 'react-native-size-matters';

const { width } = Dimensions.get('window');

export default {
  container: {
    flex: 1,
    backgroundColor: colors.black,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cell: {
    flex: 1,
  },
  logo: {
    alignItems: 'center',
  },
  controls: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    width: '88%',
  },
  control: {
    alignItems: 'center',
    width: moderateScale(width * 0.2),
  },
  title: {
    fontFamily: 'Work Sans Regular',
    color: colors.white,
    fontSize: moderateScale(font.big.size),
    lineHeight: moderateScale(font.big.height),
    textAlign: 'center',
    marginTop: moderateScale(gaps.big),
  },
  controlText: {
    fontFamily: 'Work Sans Regular',
    color: colors.white,
    fontSize: moderateScale(font.medium.size),
    lineHeight: moderateScale(font.medium.height),
    width: '100%',
    textAlign: 'center',
    marginTop: moderateScale(gaps.medium),
  },
}
