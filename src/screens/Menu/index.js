import React from 'react';
import PropTypes from 'prop-types';
import { Audio } from 'expo-av';
import { connect } from "react-redux";
import { Dimensions, Text, View } from 'react-native';
import AlertPromise from 'cc.rckt.boc/src/components/AlertPromise';
import NavItem from './navItem';
import { colors } from 'cc.rckt.boc/src/config/vars';

import { soundRequest } from 'cc.rckt.boc/src/reducers/sounds/actions';
import { firstLaunchUpdate } from 'cc.rckt.boc/src/reducers/preferences/actions';

import style from './style';

import { SvgLogo, SvgDiamonds, SvgHearts, SvgSpades } from 'cc.rckt.boc/src/components/svg';

const { width } = Dimensions.get('window');

class Menu extends React.Component {
  componentDidMount() {
    this.loadSounds();
    this.firstLaunchNotice();
  }

  firstLaunchNotice = () => {
    if (this.props.firstLaunch) {
      AlertPromise(
        {
          title: 'Hello',
          message: `This is the first time you've launched this game.\n\nTo avoid any kind of frustration, it would be better if you read the rules before playing.\n\nThank you!`,
          buttons: [
            {
              label: 'OK',
              style: 'destructive',
              state: 'resolve',
            },
          ],
          cancelable: false,
        }
      )
      .then(() => {
        this.props.disableFirstLaunchNotice();
      });
    }
  };

  loadSounds = () => {
    Audio.setIsEnabledAsync(true)
      .then(() => {
        this.props.requestSound('win', require('./../../../assets/snd/win.mp3'));
        this.props.requestSound('lose', require('./../../../assets/snd/lose.mp3'));
        this.props.requestSound('deal', require('./../../../assets/snd/deal.mp3'));
        this.props.requestSound('button', require('./../../../assets/snd/button.mp3'));
        this.props.requestSound('discard', require('./../../../assets/snd/discard.mp3'));
        this.props.requestSound('avoid', require('./../../../assets/snd/avoid.mp3'));
      });
  }

  render() {
    const { navigation: { navigate } } = this.props;

    return (
      <View style={style.container}>
        <View style={style.cell} />
        <View style={{ ...style.cell, ...style.logo }}>
          <SvgLogo width={width * 0.14} />
          <Text style={style.title} numberOfLines={1}>
            Battle of Cards
          </Text>
        </View>
        <View style={{ ...style.cell, ...style.controls }}>
          <NavItem
            onPress={() => navigate('Rules')}
            title="Rules"
          >
            <SvgHearts />
          </NavItem>
          <NavItem
            onPress={() => navigate('Settings')}
            title="Settings"
          >
            <SvgSpades fill={colors.white} />
          </NavItem>
          <NavItem
            onPress={() => navigate('Game')}
            title="Start"
          >
            <SvgDiamonds />
          </NavItem>
        </View>
      </View>
    );
  }
}

Menu.propTypes = {
  navigation: PropTypes.object,
  requestSound: PropTypes.func,
  firstLaunch: PropTypes.bool,
  disableFirstLaunchNotice: PropTypes.func,
};

export default connect(
  state => ({
    firstLaunch: state.Preferences.firstLaunch,
  }),
  dispatch => ({
    requestSound: (name, src) => dispatch(soundRequest(name, src)),
    disableFirstLaunchNotice: () => dispatch(firstLaunchUpdate(false)),
  })
)(Menu);
