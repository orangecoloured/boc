import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { View, ScrollView } from 'react-native';
import { displayFallback } from 'cc.rckt.boc/src/utils';
import ButtonStandard from 'cc.rckt.boc/src/components/ui/buttons/Standard';
import AlertPromise from 'cc.rckt.boc/src/components/AlertPromise';
import { statsPurge } from 'cc.rckt.boc/src/reducers/stats/actions';

import { TextParagraph, TextHeader } from 'cc.rckt.boc/src/components/typography';

import style from './style';

class Stats extends React.Component {
  statsReset = () => {
    this.props.statsReset();
  }

  statsResetPrompt = () => {
    AlertPromise(
      {
        title: 'Clear Stats',
        message: 'Would you like to clear your stats?',
        buttons: [
          {
            label: 'No',
            style: 'cancel',
            state: 'reject',
          },
          {
            label: 'Yes',
            style: 'destructive',
            state: 'resolve',
          },
        ],
      }
    )
    .then(() => {
      this.props.statsReset();
    })
    .catch(() => {});
  }

  calculateGamesQuit = () => {
    const { games } = this.props;

    if (!isNaN(games.total) && !isNaN(games.won) && !isNaN(games.lost)) {
      return games.total - (games.won + games.lost);
    } else {
      return 'N/A';
    }
  }

  render() {
    const { games, scores } = this.props;

    return(
      <View style={style.container}>
        <ScrollView style={style.main}>
          <View style={style.contentWrapper}>
            <TextHeader>
              Battles
            </TextHeader>
            <View style={style.row}>
              <TextParagraph>Total</TextParagraph>
              <TextParagraph>{displayFallback(games.total)}</TextParagraph>
            </View>
            <View style={style.row}>
              <TextParagraph>Survived</TextParagraph>
              <TextParagraph>{displayFallback(games.won)}</TextParagraph>
            </View>
            <View style={style.row}>
              <TextParagraph>Died</TextParagraph>
              <TextParagraph>{displayFallback(games.lost)}</TextParagraph>
            </View>
            <View style={style.row}>
              <TextParagraph>Fled</TextParagraph>
              <TextParagraph>{this.calculateGamesQuit()}</TextParagraph>
            </View>
            <View style={style.row}>
              <TextParagraph>Avoided Turns</TextParagraph>
              <TextParagraph>{displayFallback(games.avoided)}</TextParagraph>
            </View>
            <TextHeader>
              Score
            </TextHeader>
            <View style={style.row}>
              <TextParagraph>Total</TextParagraph>
              <TextParagraph>{displayFallback(scores.total)}</TextParagraph>
            </View>
            <View style={style.row}>
              <TextParagraph>Best</TextParagraph>
              <TextParagraph>{displayFallback(scores.best)}</TextParagraph>
            </View>
            <View style={style.row}>
              <TextParagraph>Worst</TextParagraph>
              <TextParagraph>{displayFallback(scores.worst)}</TextParagraph>
            </View>
            <View style={style.row}>
              <TextParagraph>Last</TextParagraph>
              <TextParagraph>{displayFallback(scores.last)}</TextParagraph>
            </View>
          </View>
        </ScrollView>
        <View style={style.bottom}>
          <ButtonStandard
            styleWrapper={style.buttonWrapper}
            styleButton={style.button}
            styleText={style.buttonText}
            label="Clear Stats"
            onPress={this.statsResetPrompt}
          />
        </View>
      </View>
    );
  }
}

Stats.propTypes = {
  games: PropTypes.object,
  scores: PropTypes.object,
  statsReset: PropTypes.func,
};

export default connect(
  state => ({
    games: state.Stats.games,
    scores: state.Stats.scores,
  }),
  dispatch => ({
    statsReset: () => dispatch(statsPurge()),
  })
)(Stats);
