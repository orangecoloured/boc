import { colors, gaps, font } from 'cc.rckt.boc/src/config/vars';
import { moderateScale } from 'react-native-size-matters';
import { viewportUnits } from 'cc.rckt.boc/src/utils';

const { vw } = viewportUnits;

export default {
  container: {
    backgroundColor: colors.black,
    flex: 1,
  },
  main: {
    flex: 1,
  },
  bottom: {
    padding: 2 * vw,
    flexDirection: 'row',
  },
  buttonWrapper: {
    flex: 1,
  },
  button: {
    backgroundColor: colors.red,
    paddingVertical: moderateScale(gaps.medium),
  },
  buttonText: {
    color: colors.white,
    textAlign: 'center',
    fontSize: moderateScale(font.medium.size),
    lineHeight: moderateScale(font.medium.height),
  },
  contentWrapper: {
    paddingHorizontal: moderateScale(gaps.medium),
    paddingTop: moderateScale(gaps.medium),
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
};
