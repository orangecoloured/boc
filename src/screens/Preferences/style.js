import { colors, gaps } from 'cc.rckt.boc/src/config/vars';
import { moderateScale } from 'react-native-size-matters';

export default {
  container: {
    backgroundColor: colors.black,
    flex: 1,
  },
  contentWrapper: {
    paddingHorizontal: moderateScale(gaps.medium),
    paddingTop: moderateScale(gaps.medium),
  },
  row: {
    paddingVertical: moderateScale(gaps.medium),
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowText: {
    marginTop: 0,
    marginBottom: 0,
  },
  volumeSlider: {
    flex: 1,
  },
  switch: {
    marginLeft: 'auto',
  },
};
