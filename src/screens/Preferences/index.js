import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { View, ScrollView, Switch, Slider } from 'react-native';
import { colors } from 'cc.rckt.boc/src/config/vars';

import { TextHeader } from 'cc.rckt.boc/src/components/typography';

import { soundPlay } from 'cc.rckt.boc/src/reducers/sounds/actions'
import { vibrationDo } from 'cc.rckt.boc/src/reducers/vibration/actions';
import { volumeUpdate, soundStateUpdate, vibrationStateUpdate } from 'cc.rckt.boc/src/reducers/preferences/actions';

import style from './style';

class Preferences extends React.Component {
  state ={
    soundEnabled: this.props.soundEnabled,
    vibrationEnabled: this.props.vibrationEnabled,
  }

  onSliderVolume = value => {
    this.props.updateVolume(value);
    this.props.playSound('button');
  }

  onSwitchSound = value => {
    this.props.updateSoundState(value);
    this.setState({
      soundEnabled: value,
    });

    if (value) {
      this.props.playSound('button');
    }
  }

  onSwitchVibration = value => {
    this.props.updateVibrationState(value);
    this.setState({
      vibrationEnabled: value,
    });

    if (value) {
      this.props.doVibration('button');
    }
  }

  render() {
    const { soundVolume } = this.props;
    const { soundEnabled, vibrationEnabled } = this.state;

    return(
      <ScrollView style={style.container}>
        <View style={style.contentWrapper}>
        <View style={style.row}>
            <TextHeader styleCustom={style.rowText}>
              Vibration
            </TextHeader>
            <Switch
              trackColor={{
                false: colors.white,
                true: colors.red,
              }}
              thumbColor={colors.red}
              onValueChange={this.onSwitchVibration}
              value={vibrationEnabled}
              style={style.switch}
            />
          </View>
          <View style={style.row}>
            <TextHeader styleCustom={style.rowText}>
              Sound
            </TextHeader>
            <Switch
              trackColor={{
                false: colors.white,
                true: colors.red,
              }}
              thumbColor={colors.red}
              onValueChange={this.onSwitchSound}
              value={soundEnabled}
              style={style.switch}
            />
          </View>
          <View style={style.row}>
            <TextHeader styleCustom={style.rowText}>
              Volume
            </TextHeader>
            <Slider
              disabled={!soundEnabled}
              minimumValue={0.1}
              maximumValue={1}
              minimumTrackTintColor={colors.red}
              maximumTrackTintColor={colors.white}
              step={0.1}
              value={soundVolume}
              style={style.volumeSlider}
              thumbTintColor={colors.red}
              onSlidingComplete={this.onSliderVolume}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

Preferences.propTypes = {
  soundEnabled: PropTypes.bool,
  soundVolume: PropTypes.number,
  vibrationEnabled: PropTypes.bool,
  playSound: PropTypes.func,
  doVibration: PropTypes.func,
  updateVolume: PropTypes.func,
  updateSoundState: PropTypes.func,
  updateVibrationState: PropTypes.func,
};

export default connect(
  state => ({
    soundEnabled: state.Preferences.sound.enabled,
    vibrationEnabled: state.Preferences.vibration,
    soundVolume: state.Preferences.sound.volume,
  }),
  dispatch => ({
    playSound: name => dispatch(soundPlay(name)),
    doVibration: patternName => dispatch(vibrationDo(patternName)),
    updateVolume: value => dispatch(volumeUpdate(value)),
    updateSoundState: value => dispatch(soundStateUpdate(value)),
    updateVibrationState: value => dispatch(vibrationStateUpdate(value)),
  })
)(Preferences);
