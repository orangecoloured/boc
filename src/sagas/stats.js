import { put, select } from "redux-saga/effects";
import { numberSafe } from 'cc.rckt.boc/src/utils';

import STATS from 'cc.rckt.boc/src/reducers/stats/types';

export function* gamesTotalIncrement() {
  const gamesTotal = yield select(state => state.Stats.games.total);

  yield put({
    type: STATS.UPDATE,
    data: {
      games: {
        total: numberSafe(gamesTotal) + 1,
      },
    },
  });
}

export function* turnsAvoidedIncrement() {
  const turnsAvoided = yield select(state => state.Stats.games.avoided);

  yield put({
    type: STATS.UPDATE,
    data: {
      games: {
        avoided: numberSafe(turnsAvoided) + 1,
      },
    },
  });
}

export function* statsUpdate(action) {
  const { score, won } = action;
  const { games, scores } = yield select(state => state.Stats);
  const gamesNew = {
    ...(won ? { won: numberSafe(games.won) + 1 } : { lost: numberSafe(games.lost) + 1 })
  };
  const scoresNew = {
    total: numberSafe(scores.total) + score,
    best: scores.best !== null ? Math.max(score, numberSafe(scores.best)) : score,
    worst: scores.worst !== null ? Math.min(score, numberSafe(scores.worst)) : score,
    last: score,
  };

  yield put({
    type: STATS.UPDATE,
    data: {
      games: gamesNew,
      scores: scoresNew,
    },
  });
}
