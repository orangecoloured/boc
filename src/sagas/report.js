import { select } from "redux-saga/effects";

export function* reportError(error) {
  const enabled = yield select(state => state.Preferences.reportErrors);

  if (enabled) {
    console.log(error);
  }
}
