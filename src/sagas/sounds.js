import { Audio } from 'expo-av';
import { Asset } from 'expo-asset';
import { put, fork, call, select } from "redux-saga/effects";
import { reportError } from './report';

import SOUNDS from 'cc.rckt.boc/src/reducers/sounds/types';

export function* soundAdd(action) {
  try {
    const sound = new Audio.Sound();

    yield call(async () => {
      await sound.loadAsync(Asset.fromModule(action.src), {
        progressUpdateIntervalMillis: 200,
        positionMillis: 0,
        shouldPlay: false,
        volume: 0,
      }, true);
    });

    yield put({
      type: SOUNDS.ADD,
      sound: {
        [action.name]: sound,
      },
    });
  } catch (e) {
    yield fork(() =>  reportError(e));
  }
}

export function* soundPlay(action) {
  try {
    const preferences = yield select(state => state.Preferences.sound);
    const sounds = yield select(state => state.Sounds);
    if (preferences.enabled && sounds[action.name]) {
      const { isLoaded } = yield call(sounds[action.name].getStatusAsync);

      if (isLoaded) {
        yield fork(async () => {
          await sounds[action.name].stopAsync();
          await sounds[action.name].setVolumeAsync(preferences.volume);
          await sounds[action.name].playAsync();
        });
      }
    }
  } catch (e) {
    yield fork(() =>  reportError(e));
  }
}
