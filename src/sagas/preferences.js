import { put } from "redux-saga/effects";

import PREFERENCES from 'cc.rckt.boc/src/reducers/preferences/types';

export function* volumeUpdate(action) {
  yield put({
    type: PREFERENCES.UPDATE,
    data: {
      sound: {
        volume: action.value,
      },
    },
  });
}

export function* soundStateUpdate(action) {
  yield put({
    type: PREFERENCES.UPDATE,
    data: {
      sound: {
        enabled: action.value,
      },
    },
  });
}

export function* vibrationStateUpdate(action) {
  yield put({
    type: PREFERENCES.UPDATE,
    data: {
      vibration: action.value,
    },
  });
}

export function* firstLaunchUpdate(action) {
  yield put({
    type: PREFERENCES.UPDATE,
    data: {
      firstLaunch: action.value,
    },
  });
}

export function* reportErrorsUpdate(action) {
  yield put({
    type: PREFERENCES.UPDATE,
    data: {
      reportErrors: action.value,
    },
  });
}
