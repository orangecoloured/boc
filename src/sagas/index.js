import { takeLatest, takeEvery } from "redux-saga/effects";

import PREFERENCES from 'cc.rckt.boc/src/reducers/preferences/types';
import SOUNDS from 'cc.rckt.boc/src/reducers/sounds/types';
import STATS from 'cc.rckt.boc/src/reducers/stats/types';
import VIBRATION from 'cc.rckt.boc/src/reducers/vibration/types';

import { volumeUpdate, soundStateUpdate, vibrationStateUpdate, firstLaunchUpdate, reportErrorsUpdate } from './preferences';
import { soundAdd, soundPlay } from './sounds';
import { gamesTotalIncrement, turnsAvoidedIncrement, statsUpdate } from './stats';
import { doVibration } from './vibration';

export default function* sagaRoot() {
  yield takeEvery(SOUNDS.REQUEST, soundAdd);
  yield takeEvery(SOUNDS.PLAY, soundPlay);
  yield takeLatest(PREFERENCES.UPDATE_VOLUME, volumeUpdate);
  yield takeLatest(PREFERENCES.UPDATE_SOUND_STATE, soundStateUpdate);
  yield takeLatest(PREFERENCES.UPDATE_VIBRATION_STATE, vibrationStateUpdate);
  yield takeLatest(PREFERENCES.UPDATE_FIRST_LAUNCH, firstLaunchUpdate);
  yield takeLatest(PREFERENCES.UPDATE_REPORT_ERRORS, reportErrorsUpdate);
  yield takeEvery(STATS.GAMES_TOTAL_INCREMENT, gamesTotalIncrement);
  yield takeEvery(STATS.TURNS_AVOIDED_INCREMENT, turnsAvoidedIncrement);
  yield takeEvery(STATS.BATCH_UPDATE, statsUpdate);
  yield takeEvery(VIBRATION.DO, doVibration);
}
