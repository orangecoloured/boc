import { fork, select } from "redux-saga/effects";
import { Vibration } from 'react-native';

const pause = {
  short: 50,
  normal: 100,
  long: 150,
};

const patterns = {
  button: [0, 50],
  deal: [0, 50, pause.normal, 50, pause.normal, 50],
  get avoid() {
    return this.deal;
  },
  won: [pause.short, 50, pause.normal, 50, pause.long, 100],
  lost: [pause.short, 100, pause.long, 50, pause.normal, 50],
};

export function* doVibration(action) {
  const enabled = yield select(state => state.Preferences.vibration);
  
  if (enabled) {
    yield fork(() => Vibration.vibrate(patterns[action.patternName]));
  }
}
