const numberSafe = number => {
  return !isNaN(number) ? number : 0;
}

export default numberSafe;
