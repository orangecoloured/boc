import { Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

const viewportUnits = {
  vw: width / 100,
  vh: height / 100,
};

viewportUnits.vmin = Math.min(viewportUnits.vw, viewportUnits.vh);
viewportUnits.vmax = Math.max(viewportUnits.vw, viewportUnits.vh);

export default viewportUnits;
