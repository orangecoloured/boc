export default (a, b, aspect, rotate) => {
  if (typeof a !== 'undefined') {
    return parseInt(a, 10);
  }

  if (typeof b !== 'undefined') {
    return parseInt(!rotate ? b * aspect : b / aspect , 10);
  }

  return undefined;
}
