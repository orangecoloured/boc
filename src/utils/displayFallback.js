const displayFallback = data => {
  if (typeof data !== 'undefined' && data !== null ) {
    return data;
  } else {
    return 'N/A';
  }
}

export default displayFallback;
