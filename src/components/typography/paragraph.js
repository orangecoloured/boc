import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';

import style from './style';

const Paragraph = ({ children, styleCustom }) => (
  <Text style={{ ...style.paragraph, ...styleCustom }}>
    {children}
  </Text>
);

Paragraph.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object,
    PropTypes.string,
    PropTypes.number,
  ]),
  styleCustom: PropTypes.object,
};

export default Paragraph;
