import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';

import style from './style';

const Header = ({ children, styleCustom }) => (
  <Text style={{ ...style.paragraph, ...style.header, ...styleCustom }}>
    {children}
  </Text>
);

Header.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object,
    PropTypes.string,
    PropTypes.number,
  ]),
  styleCustom: PropTypes.object,
};

export default Header;
