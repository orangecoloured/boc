import { colors, gaps, font } from 'cc.rckt.boc/src/config/vars';
import { moderateScale } from 'react-native-size-matters';

export default {
  paragraph: {
    fontFamily: 'Work Sans Regular',
    color: colors.white,
    fontSize: moderateScale(font.medium.size),
    lineHeight: moderateScale(font.medium.height),
    marginBottom: moderateScale(gaps.medium),
  },
  header: {
    fontSize: moderateScale(font.big.size),
    lineHeight: moderateScale(font.big.height),
    marginTop: moderateScale(gaps.medium),
    marginBottom: moderateScale(gaps.big),
  },
};
