import React from 'react';
import PropTypes from 'prop-types';
import { Path } from 'react-native-svg';
import { colors } from 'cc.rckt.boc/src/config/vars';

import SvgWrapper from './wrapper';

const Queen = ({ width, height, fill }) => (
  <SvgWrapper
    initial={{ width: 80, height: 80 }}
    display={{ width, height }}
  >
    <Path
      d="M 0 47 L 80 47 L 80 48 L 0 48 L 0 47 Z"
      fill={fill}
    />
    <Path
      d="M 0 50 L 0 80 L 5 80 L 10 75 L 10 65 L 15 60 L 65 60 L 70 65 L 70 75 L 75 80 L 80 80 L 80 50 L 0 50 Z"
      fill={fill}
    />
    <Path
      d="M 0 45 L 0 10 L 10 20 L 25 0 L 40 20 L 55 0 L 70 20 L 80 10 L 80 45 L 0 45 Z  M 10 33 C 10 31 11 30 13 30 C 14 30 15 31 15 33 C 15 34 14 35 13 35 C 11 35 10 34 10 33 Z  M 25 33 C 25 31 26 30 28 30 C 29 30 30 31 30 33 C 30 34 29 35 28 35 C 26 35 25 34 25 33 Z  M 50 33 C 50 31 51 30 53 30 C 54 30 55 31 55 33 C 55 34 54 35 53 35 C 51 35 50 34 50 33 Z  M 65 33 C 65 31 66 30 68 30 C 69 30 70 31 70 33 C 70 34 69 35 68 35 C 66 35 65 34 65 33 Z"
      fillRule="evenodd"
      fill={fill}
    />
  </SvgWrapper>
);

Queen.propTypes = {
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Queen.defaultProps = {
  fill: colors.black,
};

export default Queen;
