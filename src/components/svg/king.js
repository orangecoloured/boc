import React from 'react';
import PropTypes from 'prop-types';
import { Path } from 'react-native-svg';
import { colors } from 'cc.rckt.boc/src/config/vars';

import SvgWrapper from './wrapper';

const King = ({ width, height, fill }) => (
  <SvgWrapper
    initial={{ width: 80, height: 80 }}
    display={{ width, height }}
  >
    <Path
      d="M 0 50 L 0 80 L 5 80 L 10 75 L 10 65 L 15 60 L 65 60 L 70 65 L 70 75 L 75 80 L 80 80 L 80 50 L 0 50 Z"
      fill={fill}
    />
    <Path
      d="M 0 47 L 80 47 L 80 48 L 0 48 L 0 47 Z"
      fill={fill}
    />
    <Path
      d="M 0 45 L 0 10 L 10 20 L 25 20 L 40 0 L 55 20 L 70 20 L 80 10 L 80 45 L 0 45 Z  M 5 33 C 5 31 6 30 8 30 C 9 30 10 31 10 33 C 10 34 9 35 8 35 C 6 35 5 34 5 33 Z  M 15 33 C 15 31 16 30 18 30 C 19 30 20 31 20 33 C 20 34 19 35 18 35 C 16 35 15 34 15 33 Z  M 25 33 C 25 31 26 30 28 30 C 29 30 30 31 30 33 C 30 34 29 35 28 35 C 26 35 25 34 25 33 Z  M 38 35 C 36 35 35 34 35 33 C 35 31 36 30 38 30 C 39 30 41 30 43 30 C 44 30 45 31 45 33 C 45 34 44 35 43 35 C 41 35 39 35 38 35 Z  M 50 33 C 50 31 51 30 53 30 C 54 30 55 31 55 33 C 55 34 54 35 53 35 C 51 35 50 34 50 33 Z  M 60 33 C 60 31 61 30 63 30 C 64 30 65 31 65 33 C 65 34 64 35 63 35 C 61 35 60 34 60 33 Z  M 70 33 C 70 31 71 30 73 30 C 74 30 75 31 75 33 C 75 34 74 35 73 35 C 71 35 70 34 70 33 Z"
      fillRule="evenodd"
      fill={fill}
    />
  </SvgWrapper>
);

King.propTypes = {
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

King.defaultProps = {
  fill: colors.black,
};

export default King;
