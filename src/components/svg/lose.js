import React from 'react';
import PropTypes from 'prop-types';
import { Path } from 'react-native-svg';
import { colors } from 'cc.rckt.boc/src/config/vars';

import SvgWrapper from './wrapper';

const Lose = ({ width, height, fill }) => (
  <SvgWrapper
    initial={{ width: 80, height: 80 }}
    display={{ width, height }}
  >
    <Path
      d="M 35 60 L 35 70 L 40 65 L 45 70 L 45 60 L 35 60 Z"
      fill={fill}
    />
    <Path
      d="M 25 80 L 20 75 L 25 70 L 25 75 L 55 75 L 55 70 L 60 75 L 55 80 L 25 80 Z"
      fill={fill}
    />
    <Path
      d="M 21 65 L 21 58 L 59 58 L 59 65 L 64 58 L 70 55 L 75 42 L 75 35 L 80 25 L 80 15 L 75 5 L 65 0 L 40 5 L 15 0 L 5 5 L 0 15 L 0 25 L 5 35 L 5 42 L 10 55 L 16 58 L 21 65 Z  M 20 20 L 22 16 L 30 20 L 25 25 L 15 25 L 10 20 L 10 15 L 18 15 L 20 20 Z  M 60 20 L 58 16 L 50 20 L 55 25 L 65 25 L 70 20 L 70 15 L 63 15 L 60 20 Z  M 35 50 C 35 49 34 48 33 48 C 31 48 30 49 30 50 Q 30 49 30 53 L 35 53 Q 35 51 35 50 Z  M 50 50 C 50 49 49 48 48 48 C 46 48 45 49 45 50 Q 45 49 45 53 L 50 53 Q 50 51 50 50 Z"
      fillRule="evenodd"
      fill={fill}
    />
  </SvgWrapper>
);

Lose.propTypes = {
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Lose.defaultProps = {
  fill: colors.white,
};

export default Lose;
