import React from 'react';
import PropTypes from 'prop-types';
import { Path } from 'react-native-svg';
import { colors } from 'cc.rckt.boc/src/config/vars';

import SvgWrapper from './wrapper';

const Close = ({ width, height, fill }) => (
  <SvgWrapper
    initial={{ width: 40, height: 40 }}
    display={{ width, height }}
  >
    <Path
      d="M 18 20 L 5 33 L 7 35 L 20 22 L 33 35 L 35 33 L 22 20 L 35 7 L 33 5 L 20 18 L 7 5 L 5 7 L 18 20 Z"
      fill={fill}
    />
  </SvgWrapper>
);

Close.propTypes = {
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Close.defaultProps = {
  fill: colors.white,
};

export default Close;
