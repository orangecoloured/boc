import React from 'react';
import PropTypes from 'prop-types';
import { Path } from 'react-native-svg';
import { colors } from 'cc.rckt.boc/src/config/vars';

import SvgWrapper from './wrapper';

const Win = ({ width, height, fill }) => (
  <SvgWrapper
    initial={{ width: 80, height: 130 }}
    display={{ width, height }}
  >
    <Path
      d="M 47 110 L 55 110 L 60 105 L 47 105 L 47 110 Z"
      fill={fill}
    />
    <Path
      d="M 33 110 L 25 110 L 20 105 L 33 105 L 33 110 Z"
      fill={fill}
    />
    <Path
      d="M 35 112 L 30 112 L 30 130 L 35 125 L 40 130 L 40 112 L 45 119 L 45 105 L 35 105 L 35 112 Z"
      fill={fill}
    />
    <Path
      d="M 45 18 L 45 5 L 40 0 L 35 5 L 35 18 L 45 18 Z"
      fill={fill}
    />
    <Path
      d="M 20 30 L 20 25 L 35 20 L 45 20 L 60 25 L 60 30 L 20 30 Z"
      fill={fill}
    />
    <Path
      d="M 31 38 L 15 35 L 5 40 L 0 50 L 0 65 L 5 75 L 5 85 L 10 100 L 16 103 L 64 103 L 70 100 L 75 85 L 75 75 L 80 65 L 80 50 L 75 40 L 65 35 L 49 38 L 50 32 L 30 32 L 31 38 Z  M 48 47 L 55 45 L 40 55 L 25 45 L 32 47 L 32 48 L 40 52 L 48 48 L 48 47 Z  M 70 65 L 65 70 L 55 70 L 50 65 L 55 67 L 65 67 L 70 65 L 70 65 Z  M 30 65 L 25 70 L 15 70 L 10 65 L 15 67 L 25 67 L 30 65 L 30 65 Z  M 50 98 L 45 98 Q 45 94 45 93 C 45 91 46 90 48 90 C 49 90 50 91 50 93 Q 50 94 50 98 L 50 98 Z  M 35 98 L 30 98 Q 30 94 30 93 C 30 91 31 90 33 90 C 34 90 35 91 35 93 Q 35 94 35 98 L 35 98 Z"
      fillRule="evenodd"
      fill={fill}
    />
  </SvgWrapper>
);

Win.propTypes = {
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Win.defaultProps = {
  fill: colors.white,
};

export default Win;
