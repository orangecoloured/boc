import React from 'react';
import PropTypes from 'prop-types';
import { Path } from 'react-native-svg';
import { colors } from 'cc.rckt.boc/src/config/vars';

import SvgWrapper from './wrapper';

const Logo = ({ width, height, fill }) => (
  <SvgWrapper
    initial={{ width: 40, height: 80 }}
    display={{ width, height }}
  >
    <Path
      d="M 25 18 L 25 5 L 20 0 L 15 5 L 15 18 L 25 18 Z"
      fill={fill}
    />
    <Path
      d="M 10 32 L 15 70 L 20 80 L 25 70 L 30 32 L 10 32 Z"
      fill={fill}
    />
    <Path
      d="M 0 30 L 0 25 L 15 20 L 25 20 L 40 25 L 40 30 L 0 30 Z"
      fill={fill}
    />
  </SvgWrapper>
);

Logo.propTypes = {
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Logo.defaultProps = {
  fill: colors.white,
};

export default Logo;
