import React from 'react';
import PropTypes from 'prop-types';
import { Path } from 'react-native-svg';
import { colors } from 'cc.rckt.boc/src/config/vars';

import SvgWrapper from './wrapper';

const Clubs = ({ width, height, fill }) => (
  <SvgWrapper
    initial={{ width: 80, height: 80 }}
    display={{ width, height }}
  >
    <Path
      d="M 25 55 L 0 40 L 25 25 L 40 0 L 55 25 L 80 40 L 55 55 L 40 80 L 25 55 Z"
      fill={fill}
    />
  </SvgWrapper>
);

Clubs.propTypes = {
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Clubs.defaultProps = {
  fill: colors.black,
};

export default Clubs;
