import React from 'react';
import PropTypes from 'prop-types';
import { Path } from 'react-native-svg';
import { colors } from 'cc.rckt.boc/src/config/vars';

import SvgWrapper from './wrapper';

const Joker = ({ width, height, fill }) => (
  <SvgWrapper
    initial={{ width: 80, height: 80 }}
    display={{ width, height }}
  >
    <Path
      d="M 1 40 C 1 18 18 1 40 1 C 62 1 79 18 79 40 C 79 62 62 79 40 79 C 18 79 1 62 1 40 Z"
      fill={fill}
    />
  </SvgWrapper>
);

Joker.propTypes = {
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Joker.defaultProps = {
  fill: colors.black,
};

export default Joker;
