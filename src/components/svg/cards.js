import React from 'react';
import PropTypes from 'prop-types';
import { Path } from 'react-native-svg';
import { colors } from 'cc.rckt.boc/src/config/vars';

import SvgWrapper from './wrapper';

const Cards = ({ width, height, fill }) => (
  <SvgWrapper
    initial={{ width: 80, height: 80 }}
    display={{ width, height }}
  >
    <Path
      d="M 50 8 L 50 72 L 42 80 L 8 80 L 0 72 L 0 8 L 8 0 L 42 0 L 50 8 Z"
      fill={fill}
    />
    <Path
      d="M 50 80 L 72 80 L 80 72 L 80 8 L 72 0 L 50 0 L 56 6 L 56 74 L 50 80 Z"
      fill={fill}
    />
  </SvgWrapper>
);

Cards.propTypes = {
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Cards.defaultProps = {
  fill: colors.white,
};

export default Cards;
