import React from 'react';
import PropTypes from 'prop-types';
import { Path } from 'react-native-svg';
import { colors } from 'cc.rckt.boc/src/config/vars';

import SvgWrapper from './wrapper';

const Spades = ({ width, height, fill }) => (
  <SvgWrapper
    initial={{ width: 80, height: 80 }}
    display={{ width, height }}
  >
    <Path
      d="M 0 40 L 40 0 L 80 40 L 55 45 L 40 80 L 25 45 L 0 40 Z"
      fill={fill}
    />
  </SvgWrapper>
);

Spades.propTypes = {
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Spades.defaultProps = {
  fill: colors.black,
};

export default Spades;
