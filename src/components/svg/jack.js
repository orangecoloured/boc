import React from 'react';
import PropTypes from 'prop-types';
import { Path } from 'react-native-svg';
import { colors } from 'cc.rckt.boc/src/config/vars';

import SvgWrapper from './wrapper';

const Jack = ({ width, height, fill }) => (
  <SvgWrapper
    initial={{ width: 80, height: 80 }}
    display={{ width, height }}
  >
    <Path
      d="M 80 50 L 80 80 L 75 80 L 70 75 L 70 65 L 65 60 L 15 60 L 10 65 L 10 75 L 5 80 L 0 80 L 0 55 L 0 50 L 80 50 Z"
      fill={fill}
    />
    <Path
      d="M 0 47 L 80 47 L 80 48 L 0 48 L 0 47 Z"
      fill={fill}
    />
    <Path
      d="M 80 45 L 80 35 L 75 25 L 50 0 L 50 30 L 45 35 L 35 35 L 30 30 L 30 0 L 5 25 L 0 35 L 0 45 L 80 45 Z"
      fill={fill}
    />
    <Path
      d="M 32 0 L 32 29 L 36 33 L 44 33 L 48 29 L 48 0 L 32 0 Z"
      fill={fill}
    />
  </SvgWrapper>
);

Jack.propTypes = {
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Jack.defaultProps = {
  fill: colors.black,
};

export default Jack;
