import React from 'react';
import PropTypes from 'prop-types';
import { Path } from 'react-native-svg';
import { colors } from 'cc.rckt.boc/src/config/vars';

import SvgWrapper from './wrapper';

const Hearts = ({ width, height, fill }) => (
  <SvgWrapper
    initial={{ width: 80, height: 80 }}
    display={{ width, height }}
  >
    <Path
      d="M 0 25 L 20 0 L 40 15 L 60 0 L 80 25 L 70 55 L 40 80 L 10 55 L 0 25 Z"
      fill={fill}
    />
  </SvgWrapper>
);

Hearts.propTypes = {
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Hearts.defaultProps = {
  fill: colors.red,
};

export default Hearts;
