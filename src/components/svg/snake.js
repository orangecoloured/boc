import React from 'react';
import PropTypes from 'prop-types';
import { Path } from 'react-native-svg';
import { colors } from 'cc.rckt.boc/src/config/vars';

import SvgWrapper from './wrapper';

const Snake = ({ width, height, fill }) => (
  <SvgWrapper
    initial={{ width: 80, height: 80 }}
    display={{ width, height }}
  >
    <Path
      d="M 47 75 L 55 75 L 60 70 L 47 70 L 47 75 Z"
      fill={fill}
    />
    <Path
      d="M 33 75 L 25 75 L 20 70 L 33 70 L 33 75 Z"
      fill={fill}
    />
    <Path
      d="M 35 70 L 35 80 L 40 75 L 45 80 L 45 70 L 35 70 Z"
      fill={fill}
    />
    <Path
      d="M 16 68 L 10 65 L 5 50 L 5 40 L 0 30 L 0 15 L 5 5 L 15 0 L 15 0 L 40 5 L 65 0 L 75 5 L 80 15 L 80 30 L 75 40 L 75 50 L 70 65 L 64 68 L 16 68 Z  M 35 63 Q 35 59 35 58 C 35 56 34 55 33 55 C 31 55 30 56 30 58 Q 30 59 30 63 L 35 63 L 35 63 Z  M 50 63 Q 50 59 50 58 C 50 56 49 55 48 55 C 46 55 45 56 45 58 Q 45 59 45 63 L 50 63 L 50 63 Z  M 60 30 L 58 26 L 50 30 L 55 35 L 65 35 L 70 30 L 70 25 L 63 25 L 60 30 Z  M 20 30 L 22 26 L 30 30 L 25 35 L 15 35 L 10 30 L 10 25 L 18 25 L 20 30 Z"
      fillRule="evenodd"
      fill={fill}
    />
  </SvgWrapper>
);

Snake.propTypes = {
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Snake.defaultProps = {
  fill: colors.black,
};

export default Snake;
