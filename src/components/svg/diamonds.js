import React from 'react';
import PropTypes from 'prop-types';
import { Path } from 'react-native-svg';
import { colors } from 'cc.rckt.boc/src/config/vars';

import SvgWrapper from './wrapper';

const Diamonds = ({ width, height, fill }) => (
  <SvgWrapper
    initial={{ width: 80, height: 80 }}
    display={{ width, height }}
  >
    <Path
      d="M 40 80 L 0 40 L 40 0 L 80 40 L 40 80 Z"
      fill={fill}
    />
  </SvgWrapper>
);

Diamonds.propTypes = {
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Diamonds.defaultProps = {
  fill: colors.red,
};

export default Diamonds;
