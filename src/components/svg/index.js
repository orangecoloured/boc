export { default as SvgLogo } from "./logo";
export { default as SvgClose } from "./close";
export { default as SvgCards } from "./cards";

export { default as SvgClubs } from "./clubs";
export { default as SvgJoker } from "./joker";
export { default as SvgHearts } from "./hearts";
export { default as SvgSpades } from "./spades";
export { default as SvgDiamonds } from "./diamonds";

export { default as SvgAce } from "./ace";
export { default as SvgKing } from "./king";
export { default as SvgJack } from "./jack";
export { default as SvgSnake } from "./snake";
export { default as SvgQueen } from "./queen";

export { default as SvgWin } from "./win";
export { default as SvgLose } from "./lose";
