import React from 'react';
import PropTypes from 'prop-types';
import Svg from 'react-native-svg';

import { sideValue } from 'cc.rckt.boc/src/utils';

const Wrapper = ({ initial, display, children  }) => {
  const aspect = initial.width / initial.height;

  return (
    <Svg
      width={sideValue(display.width, display.height, aspect) || initial.width}
      height={sideValue(display.height, display.width, aspect, true) || initial.height}
      viewBox={`0 0 ${initial.width} ${initial.height}`}
    >
      {children}
    </Svg>
  );
}

Wrapper.propTypes = {
  initial: PropTypes.shape({
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired
  }),
  display: PropTypes.shape({
    width: PropTypes.number,
    height: PropTypes.number
  }),
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object,
  ]),
};

export default Wrapper;
