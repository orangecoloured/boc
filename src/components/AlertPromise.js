import { Alert } from 'react-native';

const AlertPromise = params => {
    let { cancelable, onDismiss, title, message, buttons, type } = params;

    return new Promise((resolve, reject) => {
        const buttonsArray = [];
        const getAction = function(state, payload) {
            switch(state) {
                case 'resolve': {
                    resolve(payload || true);
                    break;
                }
                case 'reject': {
                    reject(payload || false);
                    break;
                }
                default: {
                    resolve(false);
                }
            }
        }

        if (buttons) {
            for (let i = 0; i < buttons.length; i++) {
                buttonsArray.push({
                    text: buttons[i].label,
                    style: buttons[i].style || 'default',
                    onPress: () => getAction(buttons[i].state, buttons[i].payload)
                });
            }
        } else {
            buttonsArray.push({
                text: 'OK',
                style: 'default',
            });
        }

        Alert.alert(
            title || null,
            message || null,
            buttonsArray || null,
            {
                cancelable: cancelable !== undefined ? cancelable : true,
                onDismiss: onDismiss || null
            },
            type || null,
        )
    });
}

export default AlertPromise;
