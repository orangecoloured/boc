import { colors, gaps, font } from 'cc.rckt.boc/src/config/vars';
import { moderateScale } from 'react-native-size-matters';

export default {
  body: {
    paddingHorizontal: moderateScale(gaps.small),
    paddingVertical: moderateScale(gaps.xsmall),
    borderRadius: moderateScale(2),
    backgroundColor: colors.white,
    justifyContent: 'center',
  },
  label: {
    fontFamily: 'Work Sans Regular',
    color: colors.black,
    fontSize: moderateScale(font.small.size),
    lineHeight: moderateScale(font.small.height),
  },
  inactiveBody: {
    backgroundColor: colors.grey,
  },
  inactiveLabel: {
    color: colors.black,
  },
};
