import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { TouchableOpacity, View, Text } from 'react-native';

import { soundPlay } from 'cc.rckt.boc/src/reducers/sounds/actions';
import { vibrationDo } from 'cc.rckt.boc/src/reducers/vibration/actions';

import style from './style';

const Button = ({ label, onPress, styleWrapper, styleButton, styleText, disabled, playSound, doVibration }) => (
  <View style={styleWrapper}>
    <TouchableOpacity
      onPress={() => {
        if (!disabled) {
          doVibration('button');
          playSound('button');
          onPress();
        }
      }}
    >
      <View style={{ ...style.body, ...styleButton, ...(disabled ? style.inactiveBody : {}) }} pointerEvents={disabled ? 'none' : 'auto'}>
        <Text style={{ ...style.label, ...styleText, ...(disabled ? style.inactiveLabel : {}) }} numberOfLines={1}>{label}</Text>
      </View>
    </TouchableOpacity>
  </View>
);

Button.propTypes = {
  onPress: PropTypes.func,
  styleWrapper: PropTypes.object,
  styleButton: PropTypes.object,
  styleText: PropTypes.object,
  label: PropTypes.string,
  disabled: PropTypes.bool,
  playSound: PropTypes.func,
  doVibration: PropTypes.func,
};

export default connect(
  null,
  dispatch => ({
    playSound: name => dispatch(soundPlay(name)),
    doVibration: patternName => dispatch(vibrationDo(patternName)),
  })
)(Button);
