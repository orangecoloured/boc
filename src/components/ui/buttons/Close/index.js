import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, View } from 'react-native';
import { moderateScale } from 'react-native-size-matters';

import { SvgClose } from 'cc.rckt.boc/src/components/svg';

import style from './style';

const Close = ({ onPress }) => (
  <TouchableOpacity
    onPress={onPress}
  >
    <View style={style.container}>
      <SvgClose width={moderateScale(24)} />
    </View>
  </TouchableOpacity>
);

Close.propTypes = {
  onPress: PropTypes.func,
};

export default Close;
