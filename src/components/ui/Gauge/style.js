import { colors, gaps, font, opacities } from 'cc.rckt.boc/src/config/vars';
import { moderateScale } from 'react-native-size-matters';

const highlightSize = moderateScale(gaps.small / 1.5);

export default {
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    fontSize: moderateScale(font.small.size),
    lineHeight: moderateScale(font.small.height),
    color: colors.white,
    fontFamily: 'Work Sans Regular',
  },
  inactive: {
    opacity: opacities.inactive,
  },
  valueWrapper: {
    marginLeft: moderateScale(gaps.small / 2),
    paddingVertical: moderateScale(gaps.small),
  },
  marginRight: {
    marginRight: moderateScale(gaps.small / 2),
  },
  highlight: {
    width: highlightSize,
    height: highlightSize,
    backgroundColor: colors.red,
    borderRadius: highlightSize / 2,
    position: 'absolute',
    bottom: 0,
    left: '50%',
    marginLeft: -highlightSize / 2,
  },
};
