import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, Animated, Easing } from 'react-native';
import { opacities } from 'cc.rckt.boc/src/config/vars';

import style from './style';


class Gauge extends React.Component {
  state = {
    opacityDisplay: new Animated.Value(opacities.idle),
  }

  componentDidUpdate(prevProps) {
    if (this.props.value !== prevProps.value) {
      this.blink();
    }
  }

  blink = () => {
    Animated.timing(this.state.opacityDisplay).stop();

    this.setState({
      opacityDisplay: new Animated.Value(1),
    }, () => {
      Animated.timing(
        this.state.opacityDisplay,
        {
          toValue: opacities.idle,
          duration: 200,
          easing: Easing.out(Easing.quad),
          isInteraction: false,
          useNativeDriver: true,
        }
      ).start();
    });
  }

  render() {
    const { value, valueAdditional, label, icon, styleWrapper, highlight } = this.props;

    return (
      <View style={{ ...style.wrapper, ...styleWrapper }}>
        {
          icon && (
            <Animated.View
              style={{
                opacity: this.state.opacityDisplay,
              }}
            >
              {icon(style.text.fontSize)}
            </Animated.View>
          )
        }
        <View style={{ ...style.valueWrapper, ...(label ? style.marginRight : {})}}>
          <Text
            style={{ ...style.text, ...(!value ? style.inactive : {}) }}
            numberOfLines={1}
          >
            {(value || '-') + (typeof valueAdditional !== 'undefined' ? ` / ${valueAdditional || '-'}` : '')}
          </Text>
          {highlight && <View style={style.highlight} />}
        </View>
        {
          label && (
            <Animated.Text
              style={{
                ...style.text,
                opacity: this.state.opacityDisplay,
              }}
            >
              {label}
            </Animated.Text>
          )
        }
      </View>
    );
  }
}

Gauge.propTypes = {
  value: PropTypes.oneOfType([ PropTypes.number, PropTypes.string ]),
  valueAdditional: PropTypes.oneOfType([ PropTypes.number, PropTypes.string ]),
  label: PropTypes.string,
  icon: PropTypes.func,
  styleWrapper: PropTypes.object,
  highlight: PropTypes.bool,
};

export default Gauge;
