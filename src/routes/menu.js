import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';

import Game from './game';
import Menu from 'cc.rckt.boc/src/screens/Menu';
import Rules from 'cc.rckt.boc/src/screens/Rules';
import Settings from './settings';

import ButtonClose from 'cc.rckt.boc/src/components/ui/buttons/Close'

import { styleHeader } from './style';

const StackNavigator = createStackNavigator(
  {
    Game: {
      screen: ({ navigation }) => <Game screenProps={{ rootNavigation: navigation }} />,
      navigationOptions: {
        header: null,
      },
    },
    Menu: {
      screen: Menu,
      navigationOptions: {
        header: null,
      },
    },
    Rules: {
      screen: Rules,
      navigationOptions: ({ navigation }) => ({
        title: 'Rules',
        ...styleHeader,
        headerLeft: () => <ButtonClose onPress={() => { navigation.dispatch({ type: 'Navigation/BACK', key: navigation.state.key }) }} />,
      }),
    },
    Settings: {
      screen: Settings,
      navigationOptions: ({ navigation }) => ({
        title: 'Settings',
        ...styleHeader,
        headerLeft: () => <ButtonClose onPress={() => { navigation.dispatch({ type: 'Navigation/BACK', key: navigation.state.key }) }} />,
      }),
    },
  },
  {
    initialRouteName: 'Menu',
  }
);

export default createAppContainer(StackNavigator);
