import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';

import ButtonClose from 'cc.rckt.boc/src/components/ui/buttons/Close';
import Table from 'cc.rckt.boc/src/screens/Table';
import Result from 'cc.rckt.boc/src/screens/Result';

import { styleHeader } from './style';

const StackNavigator = createStackNavigator(
  {
    Table: {
      screen: Table,
      navigationOptions: ({ navigation }) => ({
        title: navigation.state.params ? navigation.state.params.title : 'Battle',
        ...styleHeader,
        headerLeft: <ButtonClose onPress={() => {
          navigation.state.params.action();
        }} />,
      }),
    },
    Result: {
      screen: Result,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    initialRouteName: 'Table',
  }
);

export default createAppContainer(StackNavigator);
