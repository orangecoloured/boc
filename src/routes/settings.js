import { createMaterialTopTabNavigator } from 'react-navigation';
import { Dimensions } from 'react-native';
import { hexToRgba } from 'cc.rckt.boc/src/utils';

import About from 'cc.rckt.boc/src/screens/About';
import Preferences from 'cc.rckt.boc/src/screens/Preferences';
import Stats from 'cc.rckt.boc/src/screens/Stats';

import { colors, opacities } from 'cc.rckt.boc/src/config/vars';

export default createMaterialTopTabNavigator(
  {
    Preferences,
    Stats,
    About,
  },
  {
    initialRouteName: 'Preferences',
    initialLayout: {
      height: 0,
      width: Dimensions.get('window').width,
    },
    tabBarOptions: {
      activeTintColor: colors.white,
      inactiveTintColor: hexToRgba(colors.white, opacities.inactive),
      upperCaseLabel: false,
      style: {
        backgroundColor: colors.black,
        elevation: 0,
      },
      indicatorStyle: {
        backgroundColor: colors.white,
      },
      labelStyle: {
        fontFamily: 'Work Sans Regular',
        color: colors.white,
      },
    },
  }
);
