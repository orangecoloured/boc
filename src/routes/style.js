import { colors } from 'cc.rckt.boc/src/config/vars';

export const styleHeader = {
  headerStyle: {
    backgroundColor: colors.black,
    elevation: 0,
  },
  headerTintColor: colors.white,
  headerTitleStyle: {
    fontFamily: 'Work Sans Regular',
    fontWeight: 'normal',
  },
};
