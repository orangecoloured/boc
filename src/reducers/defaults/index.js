const initialState = {
  deck: {
    active: [
      {suit: 'h', value: 2, name: 'Small Potion'},
      {suit: 'h', value: 3, name: 'Small Potion'},
      {suit: 'h', value: 4, name: 'Small Potion'},
      {suit: 'h', value: 5, name: 'Medium Potion'},
      {suit: 'h', value: 6, name: 'Medium Potion'},
      {suit: 'h', value: 7, name: 'Medium Potion'},
      {suit: 'h', value: 8, name: 'Big Potion'},
      {suit: 'h', value: 9, name: 'Big Potion'},
      {suit: 'h', value: 10, name: 'Big Potion'},
      {suit: 'h', value: 11, name: 'Priest'},

      {suit: 'd', value: 2, name: 'Dagger'},
      {suit: 'd', value: 3, name: 'Sword'},
      {suit: 'd', value: 4, name: 'Flail'},
      {suit: 'd', value: 5, name: 'Scimitar'},
      {suit: 'd', value: 6, name: 'War Hammer'},
      {suit: 'd', value: 7, name: 'Long Sword'},
      {suit: 'd', value: 8, name: 'Claymore'},
      {suit: 'd', value: 9, name: 'Maul'},
      {suit: 'd', value: 10, name: 'Great Sword'},
      {suit: 'd', value: 14, name: 'Paladin'},

      {suit: 's', value: 2, name: 'Fallen'},
      {suit: 's', value: 3, name: 'Goat Man'},
      {suit: 's', value: 4, name: 'Fiend'},
      {suit: 's', value: 5, name: 'Zombie'},
      {suit: 's', value: 6, name: 'Gargoyle'},
      {suit: 's', value: 7, name: 'Horned Demon'},
      {suit: 's', value: 8, name: 'Balrog'},
      {suit: 's', value: 9, name: 'Knight'},
      {suit: 's', value: 10, name: 'Viper'},
      {suit: 's', value: 11, name: 'Archmage'},
      {suit: 's', value: 12, name: 'Witch'},
      {suit: 's', value: 13, name: 'Oozedrool'},
      {suit: 's', value: 14, name: 'Butcher'},

      {suit: 'c', value: 2, name: 'Scavenger'},
      {suit: 'c', value: 3, name: 'Skeleton'},
      {suit: 'c', value: 4, name: 'Winged Fiend'},
      {suit: 'c', value: 5, name: 'Skeleton Captain'},
      {suit: 'c', value: 6, name: 'Demon'},
      {suit: 'c', value: 7, name: 'Overlord'},
      {suit: 'c', value: 8, name: 'Magma Demon'},
      {suit: 'c', value: 9, name: 'Succubus'},
      {suit: 'c', value: 10, name: 'Mage'},
      {suit: 'c', value: 11, name: 'Archbishop'},
      {suit: 'c', value: 12, name: 'Black Vex'},
      {suit: 'c', value: 13, name: 'Skeleton King'},
      {suit: 'c', value: 14, name: 'Sir Gorash'},

      {suit: 'j', value: 15, name: 'Twin Serpent'},
      {suit: 'j', value: 15, name: 'Twin Serpent'},
    ],
    drawn: [],
    discarded: [],
    animate: [],
  },
  stats: {
    hp: 20,
    ap: null,
    al: null,
  },
  game: {
    potionUsed: false,
    cardPicked: false,
    turnAvoided: false,
    turnActive: false,
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
}
