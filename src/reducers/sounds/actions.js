import SOUNDS from './types';

export const soundRequest = (name, src) => ({
  type: SOUNDS.REQUEST,
  name,
  src,
});

export const soundPlay = name => ({
  type: SOUNDS.PLAY,
  name,
});
