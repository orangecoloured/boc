const prefix = 'SOUNDS';

export default {
  ADD: `${prefix}/ADD`,
  PLAY: `${prefix}/PLAY`,
  REQUEST: `${prefix}/REQUEST`,
};
