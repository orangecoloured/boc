import SOUNDS from './types';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case SOUNDS.ADD: {
      return {
        ...state,
        ...action.sound,
      };
    }
    default: {
      return state;
    }
  }
}
