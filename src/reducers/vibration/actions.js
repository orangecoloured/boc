import VIBRATION from './types';

export const vibrationDo = patternName => ({
  type: VIBRATION.DO,
  patternName,
});
