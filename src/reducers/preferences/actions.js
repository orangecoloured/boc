import PREFERENCES from './types';

export const volumeUpdate = value => ({
  type: PREFERENCES.UPDATE_VOLUME,
  value,
});

export const soundStateUpdate = value => ({
  type: PREFERENCES.UPDATE_SOUND_STATE,
  value,
});

export const vibrationStateUpdate = value => ({
  type: PREFERENCES.UPDATE_VIBRATION_STATE,
  value,
});

export const firstLaunchUpdate = value => ({
  type: PREFERENCES.UPDATE_FIRST_LAUNCH,
  value,
});

export const reportErrorsUpdate = value => ({
  type: PREFERENCES.UPDATE_REPORT_ERRORS,
  value,
});
