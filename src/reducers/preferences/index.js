import merge from 'deepmerge';
import PREFERENCES from './types';

const initialState = {
  firstLaunch: true,
  reportErrors: false,
  vibration: true,
  sound: {
    enabled: true,
    volume: 1,
  },
  stats: {
    games: {
      total: 0,
      won: 0,
      lost: 0,
    },
    scores: {
      total: 0,
      best: 0,
      worst: 0,
      last: 0,
    },
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case PREFERENCES.UPDATE:
      return merge(state, action.data);
    default:
      return state;
  }
}
