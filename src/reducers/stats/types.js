const prefix = 'STATS';

export default {
  UPDATE: `${prefix}/UPDATE`,
  GAMES_TOTAL_INCREMENT: `${prefix}/GAMES_TOTAL_INCREMENT`,
  TURNS_AVOIDED_INCREMENT: `${prefix}/TURNS_AVOIDED_INCREMENT`,
  BATCH_UPDATE: `${prefix}/BATCH_UPDATE`,
  PURGE: `${prefix}/PURGE`,
};
