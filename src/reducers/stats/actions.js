import STATS from './types';

export const incrementGamesTotal = () => ({
  type: STATS.GAMES_TOTAL_INCREMENT,
});

export const incrementTurnsAvoided = () => ({
  type: STATS.TURNS_AVOIDED_INCREMENT,
});

export const statsPurge = () => ({
  type: STATS.PURGE,
});

export const updateBatch = (won, score) => ({
  type: STATS.BATCH_UPDATE,
  won,
  score,
});
