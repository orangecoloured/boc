import merge from 'deepmerge';
import STATS from './types';

const initialState = {
  games: {
    total: 0,
    won: 0,
    lost: 0,
    avoided: 0,
  },
  scores: {
    total: 0,
    best: null,
    worst: null,
    last: null,
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case STATS.UPDATE:
      return merge(state, action.data);
    case STATS.PURGE:
      return {
        games: { ...initialState.games },
        scores: { ...initialState.scores },
      };
    default:
      return state;
  }
}
